﻿using System;

namespace ImageReader
{
    public class Color
    {
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public byte A { get; set; }
        public bool UseAlpha { get; set; }

        private int _current;

        public Color(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }

        public Color(byte r, byte g, byte b, byte a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
            UseAlpha = true;
        }

        public Color() { }

        public void AddColor(byte b)
        {
            if (_current == 0)
                R = b;
            else if (_current == 1)
                G = b;
            else if (_current == 2)
                B = b;
            else if (_current == 3)
                A = b;

            _current++;
        }

        public Color Delta(Color next)
        {
            var r = Difference(R, next.R);
            var g = Difference(G, next.G);
            var b = Difference(B, next.B);
            var a = Difference(A, next.A);
            return new Color(r, g, b, a);
        }

        private byte Difference(byte old, byte nu)
        {
            var start = (int)old;
            var diff = (int)nu;
            var result = start;
            for (int i = start; i < start + diff; i++)
            {
                result++;
                if (result > 255)
                    result = 0;
            }
            return (byte)result;
        }

        public Color Duplicate()
        {
            var copy = new Color(R,G,B,A);
            copy.UseAlpha = UseAlpha;
            return copy;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Color))
                return false;

            var comp = (Color)obj;
            if (comp.R == R &&
                comp.G == G &&
                comp.B == B &&
                comp.A == A)
                return true;
            else
                return false;
        }

        public float ColorDistance(Color color)
        {
            var rD = Math.Abs(color.R - R);
            var gD = Math.Abs(color.G - G);
            var bD = Math.Abs(color.B - B);
            var aD = Math.Abs(color.A - A);
            var sum = rD + gD + bD + aD;
            var avg = (float)sum / (float)4;
            return avg;
        }

        public bool IsBrighterThan(Color color)
        {
            var sum = R + B + G;
            var otherSum = color.R + color.B + color.G;
            if (sum > otherSum)
                return true;
            else
                return false;
        }
        
        
    }
}
