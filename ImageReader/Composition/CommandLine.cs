﻿using ImageReader.Manipulation.CloneStamp;
using ImageReader.Manipulation.FullProgram;
using ImageReader.Manipulation.FullPrograms;
using ImageReader.Png;
using ImageReader.Roles;
using ImageReader.Testing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Composition
{
    public class CommandLine
    {
        public PngImage Image { get; set; }
        public IDataAccess Io { get; set; }
        
        public CommandLine()
        {
            Io = New.DataAccess(true);
        }

        public void Run()
        {
            LoadImage();
            ManipulateImage();
            SaveImage();
        }

        public void ManipulateImage()
        {
            //var a = new RotateTree();
            //Image = a.Run(Image);

            //var a = new AkiraGrandma();
            //Image = a.Run(Image);

            var a = new AnyPicture();
            Image = a.Run(Image);


        }

        public void LoadImage()
        {
            var data = Io.GetData();
            var loader = New.ImageLoader();
            Image = loader.Load(data);
        }

        public void SaveImage()
        {
            var enc = New.ImageEncoder();
            var data = enc.Encode(Image);
            Io.WriteData(data);
        }
        
    }
}
