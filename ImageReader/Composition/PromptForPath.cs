﻿using ImageReader.Roles;
using System;
using System.IO;

namespace ImageReader.Composition
{
    public class PromptForPath : IDataAccess
    {
        public byte[] GetData()
        {
            var p = GetPath();
            return File.ReadAllBytes(p);
        }

        public void WriteData(byte[] data)
        {
            File.WriteAllBytes(GetPath("output", false), data);
        }

        private string GetPath(string query = "image", bool check = true)
        {
            Start:
            Console.WriteLine($"Enter path to {query}");
            var path = Console.ReadLine();
            if (check)
            {
                if (!File.Exists(path))
                {
                    Console.WriteLine("Invalid path");
                    goto Start;
                }
                else if (Path.GetExtension(path).ToUpper() != ".PNG")
                {
                    Console.WriteLine("PNG only");
                    goto Start;
                }
            }
            return path;
        }
    }
}
