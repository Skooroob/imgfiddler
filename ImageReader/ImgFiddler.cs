﻿using ImageReader.Manipulation;
using ImageReader.Manipulation.BlurEffect;
using ImageReader.Manipulation.CloneStamp;
using ImageReader.Manipulation.Drawing;
using ImageReader.Manipulation.RedWaveEffect;
using ImageReader.Png;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zlib;

namespace ImageReader
{
    public class ImgFiddler
    {
        public string FilePath { get; set; }
        public byte[] Data { get; set; }
        public PngHeader ImgHeader { get; set; }
        private byte[] ImgEnd { get; set; }
        private int EndPosition { get; set; }
        private PngImage Image;

        public ImgFiddler()
        {
            ImgEnd = Encoding.ASCII.GetBytes("IEND");
        }

        public void Run()
        {
            GetPath();
            Console.WriteLine("Working...");
            Examine();
            Console.WriteLine("Done");
        }

        public void GetPath()
        {
            Start:
            Console.WriteLine("Enter path to image");
            //var path = Console.ReadLine();
            //var path = @"C:\Users\tfloyd\Pictures\jiraBots.png";
            //var path = @"C:\Users\tfloyd\Pictures\Test\redDot.png";
            //var path = @"C:\Users\tfloyd\Pictures\Test\Test.png";
            //var path = @"C:\Users\tfloyd\Pictures\softWhiteRibbon.png";
            //var path = @"C:\Users\tfloyd\Pictures\Test\cool.png";
            //var path = @"C:\Users\tfloyd\Pictures\Test\nice.png";
           // var path = @"C:\Users\tfloyd\Pictures\tree.png";


            //this has paeth filtering
           //var path = @"C:\Users\tfloyd\Pictures\dumbPicture_small.png";
           var path = @"C:\Users\tfloyd\Pictures\dumbPicture.png";



            //var path = @"C:\Users\tfloyd\Pictures\geometricRed.png";
            if (!File.Exists(path))
            {
                Console.WriteLine("Invalid path");
                goto Start;
            }
            else if (Path.GetExtension(path).ToUpper() != ".PNG")
            {
                Console.WriteLine("PNG only");
                goto Start;
            }
            else
            {
                FilePath = path;
            }
        }

        public void Examine()
        {
            Data = File.ReadAllBytes(FilePath);
            //FirstEightBytes();
            FindEnd();
            
            if (Find("IHDR", out int position))
                GetImgHeaderData(position);

            if (Find("PLTE", out int plte))
            {
                throw new NotImplementedException();
            }

            if (FindMany("IDAT", out int[] positions))
                GetColors(positions);
        }

        private bool FindMany(string chunk, out int[] positions)
        {
            positions = null;
            var result = new List<int>();
            var query = Encoding.ASCII.GetBytes(chunk);
            var len = query.Length;
            for (int i = 0; i < Data.Length; i++)
            {
                if (EndOfImage(i))
                    break;

                var comp = GetRange(i, len);
                if (CompareBytes(query, comp))
                {
                    var start = i + len;//-1
                    result.Add(start);
                }
            }

            if (result.Count > 0)
            {
                positions = result.ToArray();
                return true;
            }
            else
                return false;
        }

        private bool Find(string chunk, out int start)
        {
            start = 0;
            var query = Encoding.ASCII.GetBytes(chunk);
            var len = query.Length;
            for (int i = 0; i < Data.Length; i++)
            {
                if (EndOfImage(i))
                    break;

                var comp = GetRange(i, len);
                if (CompareBytes(query, comp))
                {
                    start = i + len -1;
                    return true;
                }
            }
            
            return false;
        }

        private bool EndOfImage(int position)
        {
            if (position >= EndPosition)
                return true;
            else
                return false;
        }

        private bool CompareBytes(byte[] A, byte[] B)
        {
            if (A.Length != B.Length)
                return false;

            for (int i = 0; i < A.Length; i++)
                if (A[i] != B[i])
                    return false;
            
            return true;
        }
        
        private byte[] GetRange(int start, int len)
        {
            var result = new byte[len];
            var inc = 0;
            for (int i = start; i < start+len; i++)
            {
                result[inc] = Data[i];
                inc++;
            }
            return result;
        }

        private byte[] Pad(byte[] original)
        {
            var result = new byte[4];
            for (int i = 0; i < result.Length; i++)
                if (i < original.Length)
                    result[i] = original[i];

            return result;
        }

        private void FindEnd()
        {
            for (int i = Data.Length - 5; i > 0; i--)
            {
                var comp = GetRange(i, 4);
                if (CompareBytes(comp, ImgEnd))
                {
                    EndPosition = i;
                    break;
                }
            }
        }

        private void GetColors(int[] positions)
        {
            //The reason you got 120 for first value:
            //the IDAT is big endian, so needs to reversed before using. You were reading
            //cycle redund. check values

            //These are all off by 1, apparently
            //for (int i = 0; i < positions.Length; i++)
            //    positions[i]++;

            //FIXED: This causes a crc error in the ihdr chunk! 
            //now the problem is invalid row-filtering types (what I called delta config)
            //which, judging from the output of pngcheck.exe, are all over the place
            var greatData = new List<byte>();
            for (int i = 0; i < positions.Length; i++)
            {
                var pos = positions[i] - 8;//go back before the name of the chunk, plus 4 more for length
                var l = GetRange(pos, 4);
                l = l.Reverse().ToArray();
                var plen = BitConverter.ToInt32(l, 0);
                var data = GetRange(positions[i], plen);
                greatData.AddRange(data);
            }
            
            var idata = greatData;

            DecompressData(idata.ToArray(), out byte[] unc);
            idata = unc.ToList();

            var bpp = 2;
            if (ImgHeader.ColorType == 2)
                bpp = 3;
            else if (ImgHeader.ColorType == 6)
                bpp = 4;

            BytesToPixels(idata.ToArray(), bpp);

            //now try some basic image manipulation
            //for (int i = 0; i < 36; i++)
            //{
            //    Console.WriteLine("Going weird...");
            //    var b = new Blur(NeatBlurPresets.AccentuateExistingTiltShift());
            //    Image = b.Run(Image);

            //    Console.WriteLine("Swizzling r/g");
            //    var s = new Swizzle();
            //    Image = s.Run(Image);

            //    Console.WriteLine("Rays of light...");
            //    var d = new RaysOfLight();
            //    Image = d.Run(Image);

            //    var wave = new RedWave();
            //    Image = wave.Run(Image);
            //}

            //Console.WriteLine("Light blur...");
            //var set = NeatBlurPresets.Subtle();
            //set.Passes = 1;
            //set.ContrastCutoff = 8;
            //set.Invert = false;
            //var b2 = new Blur(set);
            //Image = b2.Run(Image);

            //var r = new RedWave();
            //r.Passes = 6;
            //r.Feedback = true;
            //r.RedThreshold = 33;
            //r.TraversalRadius = 12;
            //Image = r.Run(Image);

            //var rays = new RaysOfLight();
            //rays.MinBrightness = new Color(190, 190, 190);
            //Image = rays.Run(Image);

            //var blur = new Blur();
            //blur.Passes = 3;
            //blur.MinContrast = 90;
            //blur.Invert = false;
            //Image = blur.Run(Image);

            //var swiz = new Swizzle();
            //Image = swiz.Run(Image);

            //for (int i = 0; i < 5; i++)
            //{
            //    Console.WriteLine("Rays of light...");
            //    var d = new RaysOfLight();
            //    Image = d.Run(Image);
            //}

            //var blender = new BlendFavoritArea();
            //blender.minX = 325;
            //blender.maxX = 357;
            //blender.minY = 173;
            //blender.maxY = 213;
            //blender.Passes = 3;
            //blender.Feedback = true;
            ////blender.InitiatingColorDistance = 60;
            ////blender.ColorDistanceDropoff = 60;
            //Image = blender.Run(Image);

            //blender = new BlendFavoritArea();
            //blender.minX = 376;
            //blender.maxX = 390;
            //blender.minY = 300;
            //blender.maxY = 317;
            //blender.Passes = 2;
            //Image = blender.Run(Image);

            //we're trying to byte off more than we can chew in one pass
            //set scale as a different param, i suppose...
            //var blender = new BlendFavoritArea();
            //blender.minX = 3259;
            //blender.maxX = 3559;
            //blender.minY = 1709;
            //blender.maxY = 2109;
            //blender.Passes = 3;
            //blender.Feedback = true;
            //blender.MaxScale = 7;
            //Image = blender.Run(Image);
            //you should do another one for her claw hand
            //blender = new BlendFavoritArea();
            //blender.minX = 3777;
            //blender.maxX = 3900;
            //blender.minY = 3050;
            //blender.maxY = 3150;
            //blender.Passes = 2;
            //blender.Feedback = true;
            //Image = blender.Run(Image);




            Console.WriteLine("Saving...");
            SaveAsPng();
        }

        private void BytesToPixels(byte[] imgData, int bytesPerPixel)
        {
            var rowIndx = 0;

            var lastPx = new Color();

            var currentRow = new PngRow();
            var rows = new List<PngRow>();
            //var currentRow = new List<PngColor>();
            // var rows = new List<List<PngColor>>();

            // var bytesPerRow = ImgHeader.Width * bytesPerPixel;
            byte[] lastByteRow = new byte[ImgHeader.Width * bytesPerPixel];
            var currentByteRow = new byte[ImgHeader.Width * bytesPerPixel];

            byte rowFilter = 0;
            bool restart = false;
            for (int i = 0; i < imgData.Length; i+=bytesPerPixel)
            {
                if (currentRow.Count % ImgHeader.Width == 0 && !restart)
                {
                    restart = true;
                    if (currentRow.Count > 0)
                    {
                        rows.Add(currentRow);
                        currentRow = new PngRow();
                    }
                    rowFilter = imgData[i];
                    currentRow.DeltaConfiguration = (int)rowFilter;
                    i -= (bytesPerPixel-1);
                    rowIndx = 0;
                    
                    lastByteRow = currentByteRow;
                    currentByteRow = new byte[ImgHeader.Width * bytesPerPixel];
                    continue;
                }

                //this won't work because we are advancing by bytesPerPixel
                //for (int p = i; p < bytesPerPixel; p++)
                //{
                //    currentByteRow[rowIndx + (p-i)] = imgData[p];
                //}

                for (int p = i; p < i + bytesPerPixel; p++)
                {
                    currentByteRow[(rowIndx * bytesPerPixel) + (p - i)] = imgData[p];
                }


                if (rowFilter == 0)
                {
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);

                    currentRow.Add(px);
                }
                else if (rowFilter == 1)
                {
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);
                    if (currentRow.Count > 0)
                    {
                        px = lastPx.Delta(px);
                    }
                    currentRow.Add(px);
                    lastPx = px;
                }
                else if (rowFilter == 2)
                {
                    //it turns out 2 is the same as one, difference being between the px ABOVE the current px

                    //just a copy of the last row
                    //however, since I'm not 100% sure of that
                    //I need to know if there's ever a case of 2 not being followed by all zeros
                    
                    var priorPx = rows.Last().Pixels[rowIndx];
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);
                    px = priorPx.Delta(px);
                    currentRow.Add(px);


                    //working code - don't delete
                    //var copy = rows.Last().Duplicate();
                    //rows.Add(copy);
                    //i += (bytesPerPixel * (ImgHeader.Width - 1));
                }
                else if (rowFilter == 4)
                {
                    int a = 0;
                    int b = 0;
                    int c = 0;
                    var px = new Color();

                    //6 for first red val?
                    //have a look at the values
                    var rawX = imgData[i];
                    var ff = imgData[i + 1];
                    var fff = imgData[i + 2];
                    var current = currentByteRow;

                    //First Paeth pixel
                    //Above: 48, 61, 31
                    //current: 41, 54, 24
                    //actual: 41, 245, 242
                    
                    //TODO: Make this bitdepth independant (currently does not work with A channel)
                   
                    for (int k = i; k < i + bytesPerPixel; k++)
                    {
                        var raw = imgData[k];
                        var inc = k - i;
                        if (rowIndx == 0)
                        {
                            //the three values for the r channel
                            // a = (int)lastByteRow[lastByteRow.Length - bytesPerPixel - 1 + inc];//left
                            // a = 0;//we can't go left

                             var fakeB = (int)lastByteRow[inc];//above

                            a = 0;
                            c = 0;
                            var above = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                            if (inc == 0)
                            {
                                b = above.R;
                            }
                            else if (inc == 1)
                            {
                                b = above.G;
                            }
                            else if (inc == 2)
                            {
                                b = above.B;
                            }
                            else if (inc == 3)
                            {
                                b = above.A;
                            }
                        }
                        else
                        {
                            var leftPixel = currentRow.Pixels[currentRow.Pixels.Count - 1];
                            var above = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                            var aboveLeft = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count - 1];

                            if (inc == 0)
                            {
                                a = leftPixel.R;
                                b = above.R;
                                c = aboveLeft.R;
                            }
                            else if (inc == 1)
                            {
                                a = leftPixel.G;
                                b = above.G;
                                c = aboveLeft.G;
                            }
                            else if (inc == 2)
                            {
                                a = leftPixel.B;
                                b = above.B;
                                c = aboveLeft.B;
                            }
                            else if (inc == 2)
                            {
                                a = leftPixel.A;
                                b = above.A;
                                c = aboveLeft.A;
                            }
                        }

                        // p := a + b - c   //initial estimate
                        var p = a + b - c;
                        var pa = Math.Abs(p - a);
                        var pb = Math.Abs(p - b);
                        var pc = Math.Abs(p - c);

                        var aDiff = ByteDiff(raw, (byte)a);
                        var bDiff = ByteDiff(raw, (byte)b);
                        var cDiff = ByteDiff(raw, (byte)c);

                        //This stupid algo chooses the wrong diff sometimes
                        if (pa <= pb && pa <= pc)
                        {
                            //use a
                            var diff = ByteDiff(raw, (byte)a);
                            px.AddColor(diff);
                        }
                        else if (pb <= pc)
                        {
                            //use b
                            var diff = ByteDiff(raw, (byte)b);

                            px.AddColor(diff);
                        }
                        else
                        {
                            //use c
                            var diff = ByteDiff(raw, (byte)c);
                            px.AddColor(diff);
                        }

                    }
                    //Paeth filter - get left, above-left, and above
                    //what if row index == 0??? then others= 0
                    
                    currentRow.Add(px);
                }
                else if(rowFilter == 3)
                {

                    var rawX = imgData[i];
                    var ff = imgData[i + 1];
                    var fff = imgData[i + 2];

                    var leftPixel = new Color();
                    var abovePixel = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                    //TODO: outfit this algo for different color types (i.e., 6)
                    //35th row

                    //33: 37, 46, 36
                    //34: 40, 52, 30

                    //everything goes to hell on line 34

                    var px = new Color();

                    if (rowIndx != 0)
                    {
                        leftPixel = currentRow.Pixels[currentRow.Pixels.Count - 1];
                    }

                    //The Average filter uses the average of the two neighboring pixels (left and above) to predict the value of a pixel.
                    // Average(x) = Raw(x) - floor((Raw(x-bpp)+Prior(x))/2)
                    for (int k = i; k < i+bytesPerPixel; k++)
                    {
                        var raw = imgData[k];
                        var inc = k - i;

                        byte l = 0;
                        byte a = 0;
                        if (inc == 0)
                        {
                           l = leftPixel.R;
                           a = abovePixel.R;
                        }
                        else if (inc == 1)
                        {
                            l = leftPixel.G;
                            a = abovePixel.G;
                        }
                        else if (inc == 2)
                        {
                            l = leftPixel.B;
                            a = abovePixel.B;
                        }
                        else if (inc == 3)
                        {
                            l = leftPixel.A;
                            a = abovePixel.A;
                        }

                        var avg = (l + a) / 2;
                        var val = ByteDiff(raw, (byte)avg);
                        px.AddColor((byte)val);
                    }

                    currentRow.Add(px);
                }
                else
                {
                    throw new NotImplementedException($"Filter type {rowFilter} not supported");
                }

                rowIndx++;
                restart = false;
            }
            rows.Add(currentRow);

            Image = new PngImage();
            Image.Header = ImgHeader;
            Image.Name = "Test";
            Image.Grid = rows;
        }
        
        private byte ByteDiff(byte a, byte b)
        {
            var start = (int)b;
            var diff = (int)a;
            var result = start;
            for (int i = start; i < start+diff; i++)
            {
                result++;
                if (result > 255)
                    result = 0;

            }
            return (byte)result;
        }
        
        public static void DecompressData(byte[] inData, out byte[] outData)
        {
            using (MemoryStream outMemoryStream = new MemoryStream())
            using (ZOutputStream outZStream = new ZOutputStream(outMemoryStream))
            using (Stream inMemoryStream = new MemoryStream(inData))
            {
                CopyStream(inMemoryStream, outZStream);
                outZStream.finish();
                outData = outMemoryStream.ToArray();
            }
        }

        public static void CopyStream(System.IO.Stream input, System.IO.Stream output)
        {
            byte[] buffer = new byte[2000];
            int len;
            while ((len = input.Read(buffer, 0, 2000)) > 0)
            {
                output.Write(buffer, 0, len);
            }
            output.Flush();
        }

        public static void CompressData(byte[] inData, out byte[] outData)
        {
            using (MemoryStream outMemoryStream = new MemoryStream())
            using (ZOutputStream outZStream = new ZOutputStream(outMemoryStream, zlibConst.Z_DEFAULT_COMPRESSION))
            using (Stream inMemoryStream = new MemoryStream(inData))
            {
                CopyStream(inMemoryStream, outZStream);
                outZStream.finish();
                outData = outMemoryStream.ToArray();
            }
        }

        private void SaveAsPng()
        {
            //save an identical copy for debugging when done
            var dat = Image.Reconstitute();
            CompressData(dat, out byte[] compressed);
            //not too sure about crc data...
            var clen = compressed.Length;
            //all ints > 8bit need to be big endian
            var lbytes = BitConverter.GetBytes(clen).Reverse().ToArray();
            var h = "IDAT";
            var chunkNameBytes = Encoding.ASCII.GetBytes(h);

            var data = new List<byte>();
            data.AddRange(lbytes);
            data.AddRange(chunkNameBytes);
            data.AddRange(compressed);
            //and CRC?
            //Idk...
            CRC.Reset();
            var crc = CRC.Crc32(compressed, 0, compressed.Length, CRC.idatCrc);
            var crcBytes = BitConverter.GetBytes(crc).Reverse();
            //we may have to reverse for endianess consistency

            var headerBytes = CreateHeaderBytes();
           

            var finalResult = new List<byte>();
            //now take the heade info
            finalResult.AddRange(headerBytes);
            //add the length of the IDAT chunk
            finalResult.AddRange(lbytes);
            //add the name of the chunk
            finalResult.AddRange(chunkNameBytes);
            //add the compressed data
            finalResult.AddRange(compressed);
            //add the crc
            finalResult.AddRange(crcBytes);

            //now add the ImgEnd chunk
            var imgEndLen = BitConverter.GetBytes(0);
            var imgEndChunkName = Encoding.ASCII.GetBytes("IEND");
            var t = new byte[0];
            CRC.Reset();
            var imgEndCRC = CRC.Crc32(t, 0, 0, CRC.iendCrc);
            var imgendCrcBytes = BitConverter.GetBytes(imgEndCRC).Reverse();
            //var imgEndCRC = Encoding.ASCII.GetBytes("®B`‚");
            //var endBytes = new List<byte>();
            //endBytes.AddRange(headerBytes);
            //endBytes.AddRange(imgEndLen);
            //endBytes.AddRange(imgEndChunkName);
            //endBytes.AddRange(imgEndCRC);

            finalResult.AddRange(imgEndLen);
            finalResult.AddRange(imgEndChunkName);
            finalResult.AddRange(imgendCrcBytes);

            //out of morbid curousity, write the bytes to disk
            File.WriteAllBytes(@"C:\Users\tfloyd\Pictures\Test\Test.png", finalResult.ToArray());

        }

        private byte[] CreateHeaderBytes()
        {
            //The HEADER LENGTH IS INVALID! Needs to be 25 bytes
            Find("IHDR", out int position);
            var hStart = GetRange(0, position - 7);
            //back 4 for IHDR
            //back 4 for IHDR length (4 bytes)
            //don't add it yet
            
            var hdrData = new List<byte>();
            var width = BitConverter.GetBytes(ImgHeader.Width).Reverse().ToArray();
            hdrData.AddRange(width);
            var height = BitConverter.GetBytes(ImgHeader.Height).Reverse().ToArray();
            hdrData.AddRange(height);
            var bd = BitConverter.GetBytes(ImgHeader.BitDepth).First(k => k != 0);
            hdrData.Add(bd);
            var ct = BitConverter.GetBytes(ImgHeader.ColorType).FirstOrDefault(k => k != 0);
            hdrData.Add(ct);
            var cm = BitConverter.GetBytes(ImgHeader.CompressionMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(cm);
            var fm = BitConverter.GetBytes(ImgHeader.FilterMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(fm);
            var im = BitConverter.GetBytes(ImgHeader.InterlaceMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(im);

            var len = hdrData.Count;
            var lenBytes = BitConverter.GetBytes(len).Reverse().ToArray();
            var ihdr = Encoding.ASCII.GetBytes("IHDR");
            
            //now 4 bytes for the crc... no idea wtf that is
            //https://stackoverflow.com/questions/24082305/how-is-png-crc-calculated-exactly
            //I believe I will simply copy and paste the code from this question
            CRC.Reset();
            var crc = CRC.Crc32(hdrData.ToArray(), 0, hdrData.Count, CRC.ihdrCrc);
            var crcBytes = BitConverter.GetBytes(crc).Reverse();//new
            
            var finalResult = new List<byte>();
            //now take the preceding header data
            finalResult.AddRange(hStart);
            //now add the len
            finalResult.AddRange(lenBytes);
            //now the chunk name
            finalResult.AddRange(ihdr);
            //now the data
            finalResult.AddRange(hdrData);
            //and now the crc
            finalResult.AddRange(crcBytes);
            
            return finalResult.ToArray();
        }

        private void GetImgHeaderData(int startIndex)
        {
            //All numbers encoded in big-endian
            ImgHeader = new PngHeader();
            startIndex++;
            var w = GetRange(startIndex, 4).Reverse().ToArray();
            ImgHeader.Width = BitConverter.ToInt32(w, 0);
            var h = GetRange(startIndex + 4, 4).Reverse().ToArray();
            ImgHeader.Height = BitConverter.ToInt32(h, 0);
            var bd = GetRange(startIndex + 8, 1).Reverse().ToArray();
            bd = Pad(bd);
            ImgHeader.BitDepth = BitConverter.ToInt32(bd, 0);
            var ct = GetRange(startIndex + 9, 1).Reverse().ToArray();
            ct = Pad(ct);
            ImgHeader.ColorType = BitConverter.ToInt32(ct, 0);
            var cm = GetRange(startIndex + 10, 1).Reverse().ToArray();
            cm = Pad(cm);
            ImgHeader.CompressionMethod = BitConverter.ToInt32(cm, 0);
            var fm = GetRange(startIndex + 11, 1).Reverse().ToArray();
            fm = Pad(fm);
            ImgHeader.FilterMethod = BitConverter.ToInt32(fm, 0);
            var im = GetRange(startIndex + 12, 1).Reverse().ToArray();
            im = Pad(im);
            ImgHeader.InterlaceMethod = BitConverter.ToInt32(im, 0);
        }

        private void FirstEightBytes()
        {
            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine($"{Data[i]}");
            }
        }
    }
}
