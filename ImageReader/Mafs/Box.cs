﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Mafs
{
    public class Box
    {
        public int minX;
        public int maxX;
        public int minY;
        public int maxY;

        public Box()
        {

        }

        public Box(int mX,int mmX,int mY,int mmY)
        {
            minX = mX;
            maxX = mmX;
            minY = mY;
            maxY = mmY;
        }
    }
}
