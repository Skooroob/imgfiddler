﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Mafs
{
    public class Vec2
    {
        public int X { get; set; }
        public int Y { get; set; }
        public double dX { get; set; }
        public double dY { get; set; }

        public Vec2() { }
        public Vec2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Vec2(double dx, double dy)
        {
            dX = dx;
            dY = dy;
            X = (int)dx;
            Y = (int)dY;
        }
        
        public static double Distance(Vec2 A, Vec2 B)
        {
            var distance = Math.Sqrt(Math.Pow((A.X - B.X), 2) + Math.Pow((A.Y - B.Y), 2));

            return distance;
        }

        public static double Distance(double x, double y, Vec2 B)
        {
            return Math.Sqrt(Math.Pow((x - B.X), 2) + Math.Pow((y - B.Y), 2));
        }

        public static double Distance(double x, double y, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x - x2), 2) + Math.Pow((y - y2), 2));
        }

        public override bool Equals(Object obj)
        {
            return obj is Vec2 && this == (Vec2)obj;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
        public static bool operator ==(Vec2 a, Vec2 b)
        {
            return a.X == b.X && 
                a.Y == b.Y &&
                a.dX == b.dX &&
                a.dY == b.dY;
        }
        public static bool operator !=(Vec2 a, Vec2 b)
        {
            return !(a == b);
        }

        public static Vec2 operator +(Vec2 a, Vec2 b)
        {
            var x = a.X + b.X;
            var y = a.Y + b.Y;
            return new Vec2(x, y);
        }

        //public static int Dot(Vec2 a, Vec2 b)
        //{
        //    var x = a.X * b.X;
        //    var y = a.Y * b.Y;
        //    return x + y;
        //}

        public Vec2 OppositeDirection()
        {
            var vec = new Vec2(-dX, -dY);
            return vec;
        }

        public Vec2 Perpendicular()
        {
            var vec = new Vec2(dY, -dX);
            return vec;
        }

        public bool SamePlace(Vec2 other)
        {
            if (X == other.X && Y == other.Y)
                return true;
            else
                return false;
        }

        public double Magnitude { get { return Math.Sqrt(Dot(this, this)); } }

        public static double Dot(Vec2 A, Vec2 B, bool position = true)
        {
            if (position)
                return A.X * B.X + A.Y * B.Y;
            else
                return A.dX * B.dX + A.dY * B.dY;
        }
        public static double Cross(Vec2 A, Vec2 B, bool position = true)
        {
            if (position)
                return A.X * B.Y - A.Y * B.X;
            else
                return A.dX * B.dY - A.dY * B.dX;
        }

        /// <summary>
        /// Returns the angle between two vectors
        /// </summary>
        public static double GetAngle(Vec2 A, Vec2 B, bool position = false)
        {
            // |A·B| = |A| |B| COS(θ)
            // |A×B| = |A| |B| SIN(θ)
            
            return Math.Atan2(Cross(A, B, false), Dot(A, B, false));
        }

        public void Normalize()
        {
            var denom = Math.Sqrt(Math.Pow(dX, 2) + Math.Pow(dY, 2));
            var factor = 1.0 / denom;
            dX *= factor;
            dY *= factor;
        }

        public void AsDirection()
        {
            dX = X;
            dY = Y;
        }
    }
}
