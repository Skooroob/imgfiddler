﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Png;

namespace ImageReader.Manipulation
{
    public class Blur : IManipulator
    {
        private PngImage _source;
        //300 passes at 65, inverted is a perfect combo!
        public int Passes = 200;//how many times should the effect be applied
        /// <summary>
        /// 0 to 255. _minContrast is a cutoff to only apply the effect to certain ranges of contrast
        /// </summary>
        public int MinContrast = 65;//low  is very blurry. high is not so blurry
        public bool Invert = true;//if true: preserve contrast, blurry areas get blurrier
                                    //TODO: Spread
        public Blur() { }

        public Blur(BlurSettings presets)
        {
            Passes = presets.Passes;
            MinContrast = presets.ContrastCutoff;
            Invert = presets.Invert;
        }

        public PngImage Run(PngImage source)
        {
            _source = source;
            for (int p = 0; p < Passes; p++)
            {
                Console.WriteLine($"{p}/{Passes}");
                Manip();
            }
            return _source;
        }
        
        private void Manip()
        {
            var result = new PngImage();
            result.Header = _source.Header;
            result.Name = _source.Name;
            for (int i = 0; i < _source.Grid.Count; i++)
            {
                var row = _source.Grid[i];
                var resRow = new PngRow();
                resRow.DeltaConfiguration = row.DeltaConfiguration;
                for (int k = 0; k < row.Pixels.Count; k++)
                {
                    var px = row.Pixels[k].Duplicate();
                    var adjacents = AdjacentPixels(i, k);
                    var adjAvg = Average(adjacents);
                    if (Invert)
                    {
                        if (adjAvg.Equals(px) || px.ColorDistance(adjAvg) > MinContrast)
                        {
                            resRow.Add(px);
                            continue;
                        }
                    }
                    else
                    {
                        if (adjAvg.Equals(px) || px.ColorDistance(adjAvg) < MinContrast)
                        {
                            resRow.Add(px);
                            continue;
                        }
                    }

                   
                    var l = new List<Color>() { adjAvg, px};
                    var fullAvg = Average(l);
                    px.R = fullAvg.R;
                    px.G = fullAvg.G;
                    px.B = fullAvg.B;
                    resRow.Add(px);
                }
                result.Grid.Add(resRow);
            }
            _source = result;
        }

        /// <summary>
        /// TODO: Accept normalized vector and length. Consider making it part of a lib for re-use
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private List<Color> AdjacentPixels(int row, int column)
        {
            var result = new List<Color>();
            PngRow previousRow = new PngRow();
            bool notLeft = false;
            bool notRight = false;

            if (column != _source.Header.Width - 1)
            {
                notRight = true;
            }
            if (column != 0)
            {
                notLeft = true;
            }

            //px above
            if (row != 0)
            {
                previousRow = _source.Grid[row - 1];
                var above = previousRow.Pixels[column];
                result.Add(above);
                if (notLeft)
                {
                    var topLeft = previousRow.Pixels[column - 1];
                    result.Add(topLeft);
                }
                if (notRight)
                {
                    var topRight = previousRow.Pixels[column + 1];
                    result.Add(topRight);
                }
            }
            //px on same row
            if (notLeft)
            {
                var left = _source.Grid[row].Pixels[column - 1];
                result.Add(left);
            }
            if (notRight)
            {
                var right = _source.Grid[row].Pixels[column + 1];
                result.Add(right);
            }

            //px below
            if (row != _source.Grid.Count - 1)
            {
                var nextRow = _source.Grid[row + 1];
                var below = nextRow.Pixels[column];
                result.Add(below);
                if (notLeft)
                {
                    var dLeft = nextRow.Pixels[column - 1];
                    result.Add(dLeft);
                }
                if (notRight)
                {
                    var dRight = nextRow.Pixels[column + 1];
                    result.Add(dRight);
                }
            }
            return result;
        }

        private Color Average(List<Color> colors)
        {
            var rSum = 0;
            var gSum = 0;
            var bSum = 0;
            foreach (var color in colors)
            {
                rSum += color.R;
                gSum += color.G;
                bSum += color.B;
            }
            var rAvg = (byte)(rSum / colors.Count);
            var gAvg = (byte)(gSum / colors.Count);
            var bAvg = (byte)(bSum / colors.Count);
            var result = new Color(rAvg, gAvg, bAvg);
            return result;
        }
    }
}
