﻿
namespace ImageReader.Manipulation
{
    public class BlurSettings
    {
        public int Passes { get; set; }
        public int ContrastCutoff { get; set; }
        public bool Invert { get; set; }
    }
}
