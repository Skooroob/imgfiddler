﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.BlurEffect
{
    public static class NeatBlurPresets
    {
        public static BlurSettings AccentuateExistingTiltShift()
        {
            var set = new BlurSettings();
            set.Passes = 300;
            set.ContrastCutoff = 65;
            set.Invert = true;
            return set;
        }

        public static BlurSettings Subtle()
        {
            var set = new BlurSettings();
            set.Passes = 2;
            set.ContrastCutoff = 12;
            set.Invert = false;
            return set;
        }
    }
}
