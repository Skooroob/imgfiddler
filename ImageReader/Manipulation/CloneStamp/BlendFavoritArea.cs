﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Mafs;
using ImageReader.Png;

namespace ImageReader.Manipulation.CloneStamp
{
    public class BlendFavoritArea : IManipulator
    {
        public bool ReduceMinDist = false;

        public int minX;
        public int maxX;
        public int minY;
        public int maxY;
        private PngImage _source;

        //public something Stamp
        private Stamp MyStamp = new Stamp();
        private bool _stampSet = false;

        public int TraversalRadiusX = 15;
        public int TraversalRadiusY = 15;
        public double InitiatingColorDistance = 50;
        public int Passes = 1;
        public int MinDist = 8;
        private List<Vec2> _usedPositions;

        public bool Feedback = false;

        public float ColorDistanceDropoff = 60;

        public int MinScale = 1;
        public int MaxScale = 3;
        public double Frequency = 0.04;

        public BlendFavoritArea()
        {

        }

        public BlendFavoritArea(Stamp stamp)
        {
            MyStamp = stamp;
            _stampSet = true;

            var rad = stamp.StampRadius;
            TraversalRadiusX = (int)rad;
            TraversalRadiusY = (int)rad;
        }
        

        public PngImage Run(PngImage source)
        {
            if (Feedback)
                Console.WriteLine("BlendFavoritArea");

            _source = source;
            if(!_stampSet)
                GetStamp();

            for (int i = 0; i < Passes; i++)
            {
                

                if (Feedback)
                    Console.WriteLine($"{i}/{Passes}");
                ApplyStampInRandomPlaces();
            }
            
            return _source;
        }

        public void GetStamp()
        {
            for (int y = minY; y < maxY && y < _source.Grid.Count; y++)
            {
                var row = _source.Grid[y];
                for (int x = minX; x < maxX; x++)
                {
                    var position = new Vec2(x, y);
                    var color = _source.Grid[y].Pixels[x].Duplicate();
                    MyStamp.Pixels.Add(position, color);
                }
            }
            MyStamp.GetAvgColor();
            MyStamp.CalcluateCenter();
        }

        public void ApplyStampInRandomPlaces()
        {
            _usedPositions = new List<Vec2>();
            if (!_stampSet)
            {
                TraversalRadiusX = maxX - minX;
                TraversalRadiusY = maxY - minY;
            }
          

            var rand = new Random();
            for (int y = 0; y < _source.Grid.Count; y+= TraversalRadiusY/4)
            {
                for (int x = 0; x < _source.Grid[y].Pixels.Count; x+= TraversalRadiusX/4)
                {
                    //??
                    var color = _source.Grid[y].Pixels[x];
                    var px = MyStamp.AverageColor;

                    //if (y - TraversalRadiusY / 2 < 0 ||
                    //    x - TraversalRadiusX / 2 < 0)
                    //    continue;


                    if (color.ColorDistance(px) < InitiatingColorDistance && rand.NextDouble() < Frequency)
                    {
                        var pos = new Vec2(x, y);
                        if (CloseToUsedPositions(_usedPositions, pos))
                            continue;

                        //Draw the stamp
                        var size = rand.Next(MinScale,MaxScale);
                        if (rand.NextDouble() < 0.003)
                            size *= 2;

                        DrawStamp(x, y, size);
                        _usedPositions.Add(pos);
                    }
                }
            }
        }



        public void DrawStamp(int x, int y, int scale)
        {
            var xstart = x;// = x - (TraversalRadiusX / 2);
            var xend = x + TraversalRadiusX * scale;// (TraversalRadiusX / 2);
            if (xend > _source.Header.Width)
                xend = _source.Header.Width;

            var ystart = y;// - (TraversalRadiusY / 2);
            var yend = y + TraversalRadiusY * scale;// (TraversalRadiusY / 2);
            if (yend > _source.Header.Height)
                yend = _source.Header.Height;


            Parallel.For(ystart, yend, dy => 
            {
                var yAdv = (dy - ystart) / scale;
                for (int dx = xstart; dx < xend; dx++)
                {
                    var xAdv = (dx - xstart) / scale;
                    var positionX = MyStamp.UpperLeft.X + xAdv;
                    var positionY = MyStamp.UpperLeft.Y + yAdv;
                    var stampPosition = new Vec2(positionX, positionY);
                    var stampColor = MyStamp.ClosestColor(stampPosition);
                    var gridColor = _source.Grid[dy].Pixels[dx];

                    BlendStamp(stampColor, gridColor, stampPosition);
                    
                }

            });
            

        }

        private bool CloseToUsedPositions(List<Vec2> used, Vec2 position)
        {
            foreach (var uPos in used)
            {
                var dist = Vec2.Distance(position, uPos);
                if (dist < MinDist)
                    return true;
            }
            return false;
        }

        public void BlendStamp(Color stampColor, Color gridColor, Vec2 stampColorPosition)
        {
            var blendFac = 1.0;
            var colDist = stampColor.ColorDistance(gridColor);

            if (colDist > ColorDistanceDropoff)
                return;

            blendFac = colDist / ColorDistanceDropoff;
            blendFac = 1.0 - blendFac;
            
            //draw in the shape of a circle to obfuscate being from a square
            var dist = Vec2.Distance(MyStamp.Center, stampColorPosition);
            if (dist > MyStamp.StampRadius)
                return;

            var rDist = Math.Abs(stampColor.R - gridColor.R);
            var gDist = Math.Abs(stampColor.G - gridColor.G);
            var bDist = Math.Abs(stampColor.B - gridColor.B);
            var rFac = 1 - ((double)rDist / (double)255);
            var gFac = 1 - ((double)gDist / (double)255);
            var bFac = 1 - ((double)bDist / (double)255);
            var rAdd = (int)(rDist * rFac) * blendFac;
            var gAdd = (int)(gDist * gFac) * blendFac;
            var bAdd = (int)(bDist * bFac) * blendFac;

            if (gridColor.R > stampColor.R)
                gridColor.R = (byte)(gridColor.R - rAdd);
            else
                gridColor.R = (byte)(gridColor.R + rAdd);

            if (gridColor.G > stampColor.G)
                gridColor.G = (byte)(gridColor.G - gAdd);
            else
                gridColor.G = (byte)(gridColor.G + gAdd);

            if (gridColor.B > stampColor.B)
                gridColor.B = (byte)(gridColor.B - bAdd);
            else
                gridColor.B = (byte)(gridColor.B + bAdd);
        }
    }
}
