﻿using ImageReader.Mafs;
using ImageReader.Png;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.CloneStamp
{
    public abstract class CloneStamper
    {

        public float ColorDistanceDropoff = 60;

        public Stamp GetStamp(Box box, PngImage source)
        {
            var stamp = new Stamp();
            bool first = true;

            for (int y = box.minY; y < box.maxY && y < source.Grid.Count; y++)
            {
                var row = source.Grid[y];
                for (int x = box.minX; x < box.maxX; x++)
                {
                    var position = new Vec2(x, y);

                    if (first)
                    {
                        first = false;
                        stamp.UpperLeft = position;
                    }
                    
                    var color = source.Grid[y].Pixels[x].Duplicate();
                    stamp.Pixels.Add(position, color);
                }
            }
            stamp.GetAvgColor();
            //stamp.CalcluateCenter();
            var width = box.maxX - box.minX;
            var height = box.maxY - box.minY;
            stamp.Width = width;
            stamp.Height = height;

            var midX = (int)Math.Round((double)(width / 2)) + box.minX;
            var midY = (int)Math.Round((double)(height / 2)) + box.minY;
            stamp.Center = new Vec2(midX, midY);
            stamp.StampRadius = Vec2.Distance(stamp.Center, stamp.UpperLeft) * 0.8f;

            return stamp;
        }
        
        public void BlendColor(Color stampColor, Color gridColor, Vec2 stampColorPosition, Stamp stamp)
        {
            var blendFac = 1.0;
            var colDist = stampColor.ColorDistance(gridColor);

            if (colDist > ColorDistanceDropoff)
                return;

            blendFac = colDist / ColorDistanceDropoff;
            blendFac = 1.0 - blendFac;

            //draw in the shape of a circle to obfuscate being from a square
            var dist = Vec2.Distance(stamp.Center, stampColorPosition);
            if (dist > stamp.StampRadius)
                return;

            var rDist = Math.Abs(stampColor.R - gridColor.R);
            var gDist = Math.Abs(stampColor.G - gridColor.G);
            var bDist = Math.Abs(stampColor.B - gridColor.B);
            var rFac = 1 - ((double)rDist / (double)255);
            var gFac = 1 - ((double)gDist / (double)255);
            var bFac = 1 - ((double)bDist / (double)255);
            var rAdd = (int)(rDist * rFac) * blendFac;
            var gAdd = (int)(gDist * gFac) * blendFac;
            var bAdd = (int)(bDist * bFac) * blendFac;

            if (gridColor.R > stampColor.R)
                gridColor.R = (byte)(gridColor.R - rAdd);
            else
                gridColor.R = (byte)(gridColor.R + rAdd);

            if (gridColor.G > stampColor.G)
                gridColor.G = (byte)(gridColor.G - gAdd);
            else
                gridColor.G = (byte)(gridColor.G + gAdd);

            if (gridColor.B > stampColor.B)
                gridColor.B = (byte)(gridColor.B - bAdd);
            else
                gridColor.B = (byte)(gridColor.B + bAdd);
        }

        public Vec2[] GetRoundedPositions(double x, double y)
        {
            double altX = 0;
            double xRemainder = 0;
            var roundX = Math.Round(x);
            if (roundX > x)
            {
                xRemainder = roundX - x;
                altX = roundX - 1;
            }
            else if (roundX < x)
            {
                xRemainder = x - roundX;
                altX = roundX + 1;
            }
            else
                altX = roundX;

            double altY = 0;
            double yRemainder = 0;
            var roundY = Math.Round(y);
            if (roundY > y)
            {
                yRemainder = roundY - y;
                altY = roundY - 1;
            }
            else if (roundY < y)
            {
                yRemainder = y - roundY;
                altY = roundY + 1;
            }
            else
                altY = roundY;

            bool horizontal = true;
            //Needs to shift either horizontally, or vertically. Not both
            if (Math.Abs(xRemainder) > Math.Abs(yRemainder))
                horizontal = true;
            else if (Math.Abs(xRemainder) < Math.Abs(yRemainder))
                horizontal = false;
            else
                horizontal = true;

            var one = new Vec2((int)roundX, (int)roundY);
            if (altX == roundX && altY == roundY)
                return new Vec2[] { one };
            else
            {
                if (horizontal)
                {
                    var two = new Vec2((int)altX, (int)roundY);
                    return new Vec2[] { one, two };
                }
                else
                {
                    var two = new Vec2((int)roundX, (int)altY);
                    return new Vec2[] { one, two };
                }
            }
        }
    }
}
