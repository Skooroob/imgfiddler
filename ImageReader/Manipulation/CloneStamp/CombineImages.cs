﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Mafs;
using ImageReader.Png;
using ImageReader.Roles;
using ImageReader.Testing;

namespace ImageReader.Manipulation.CloneStamp
{
    public class CombineImages : IManipulator
    {
        public PngImage Source { get; set; }
        public PngImage Sample { get; set; }

        private Stamp MyStamp = new Stamp();

        public int minX = 130;
        public int maxX = 512;
        public int minY = 150;
        public int maxY = 370;

        public PngImage Run(PngImage source)
        {
            Source = source;
            GetSample();
            GetStampFromSample();

            //var blender = new BlendFavoritArea(MyStamp);
            //blender.InitiatingColorDistance = 60;
            //blender.ColorDistanceDropoff = 69;
            //blender.Passes = 5;
            //var img = blender.Run(Source);

            var blender = new BlendFavoritArea(MyStamp);
            blender.InitiatingColorDistance = 60;
            blender.ColorDistanceDropoff = 69;
            blender.Passes = 1;
            blender.minX = minX;
            blender.minY = minY;
            blender.maxX = maxX;
            blender.maxY = maxY;
            var img = blender.Run(Source);

            //var mark = new MarkImage(MyStamp);
            //mark.X = 30;
            //mark.Y = 30;
            //mark.Scale = 2;
            //var img = mark.Run(Source);

            return img;
        }

        private void GetSample()
        {
            var getter = new UseSchema("testSettings2.json");
            var data = getter.GetData();
            var loader = New.ImageLoader();
            Sample = loader.Load(data);
        }

        private void GetStampFromSample()
        {
            for (int y = minY; y < maxY && y < Sample.Grid.Count; y++)
            {
                var row = Sample.Grid[y];
                for (int x = minX; x < maxX; x++)
                {
                    var position = new Vec2(x, y);
                    var color = Sample.Grid[y].Pixels[x];
                    MyStamp.Pixels.Add(position, color);
                }
            }
            MyStamp.GetAvgColor();
            MyStamp.CalcluateCenter();
        }



    }
}
