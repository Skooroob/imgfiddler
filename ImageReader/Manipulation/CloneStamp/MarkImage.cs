﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Mafs;
using ImageReader.Png;

namespace ImageReader.Manipulation.CloneStamp
{
    /// <summary>
    /// Full opacity stamping. For debugging purposes
    /// </summary>
    public class MarkImage : CloneStamper, IManipulator
    {
        public Stamp MyStamp { get; set; }
        private PngImage _source { get; set; }

        public int TraversalRadiusX;
        public int TraversalRadiusY;

        public int X { get; set; }
        public int Y { get; set; }
        public int Scale { get; set; }

        private Box _box;
        bool _useBox;

        public MarkImage(Stamp stamp)
        {
            MyStamp = stamp;
            TraversalRadiusX = (int)stamp.StampRadius;
            TraversalRadiusY = (int)stamp.StampRadius;
            Scale = 1;
        }

        public MarkImage(Box box)
        {
            _box = box;
            _useBox = true;
        }

        public PngImage Run(PngImage source)
        {
            if (_useBox)
            {
                MyStamp = GetStamp(_box, source);
                TraversalRadiusX = (int)MyStamp.StampRadius;
                TraversalRadiusY = (int)MyStamp.StampRadius;
                Scale = 1;
            }

            _source = source;
            DrawStamp(X, Y, Scale);
            return _source;
        }


        public void DrawStamp(int x, int y, int scale)
        {
            var xstart = x;// = x - (TraversalRadiusX / 2);
            var xend = x + MyStamp.Width * scale;// (TraversalRadiusX / 2);
            if (xend > _source.Header.Width)
                xend = _source.Header.Width;

            var ystart = y;// - (TraversalRadiusY / 2);
            var yend = y + MyStamp.Height * scale;// (TraversalRadiusY / 2);
            if (yend > _source.Header.Height)
                yend = _source.Header.Height;


            var lastRow = new List<Color>();
            var currentRow = new List<Color>();

            for (int dy = ystart; dy < yend; dy++)
            {
                
                var yAdv = (dy - ystart) / scale;
                for (int dx = xstart; dx < xend; dx++)
                {
                    var xAdv = (dx - xstart) / scale;
                    var positionX = MyStamp.UpperLeft.X + xAdv;
                    var positionY = MyStamp.UpperLeft.Y + yAdv;
                    var stampPosition = new Vec2(positionX, positionY);
                    var col = MyStamp.ClosestColor(stampPosition);
                    var ecol = _source.Grid[dy].Pixels[dx];

                    ecol.R = col.R;
                    if (ecol.R - 10 > 0)
                        ecol.R -= 10;

                    ecol.G = col.G;
                    if (ecol.G - 10 > 0)
                        ecol.G -= 10;

                    ecol.B = col.B;
                    if (ecol.B - 10 > 0)
                        ecol.B -= 10;

                    currentRow.Add(col);
                }
            }


        }

        private bool RepeatingRows(List<Color> lastRow, List<Color> currentRow)
        {
            if (lastRow.Count == 0 || currentRow.Count == 0)
                return false;

            if (lastRow.Count != currentRow.Count)
                return false;

            for (int r = 0; r < lastRow.Count; r++)
                if (!lastRow[r].Equals(currentRow[r]))
                    return false;

            return true;
        }

    }
}
