﻿using System;
using System.Linq;
using ImageReader.Mafs;
using ImageReader.Png;

namespace ImageReader.Manipulation.CloneStamp
{
    public class RotationsAndScales : CloneStamper, IManipulator
    {
        public PngImage Source { get; set; }
        public Box MyBox { get; set; }
        public Stamp MyStamp { get; set; }
        public bool DropWhite { get; set; }
        
        public RotationsAndScales()
        {
            MyBox = new Box(0, 10, 0, 10);
        }

        public RotationsAndScales(Box box)
        {
            MyBox = box;
        }

        public PngImage Run(PngImage source)
        {
            Source = source;
            MyStamp = GetStamp(MyBox, Source);

          //  DrawStamp(500 + (int)i * 10, 500 + (int)i * 5, 2.0 - i / 20, 45 + i * .2);
          //All these params should be in the ctor
            for (double i = 1; i < 33; i++)
            {
                DrawStamp(500+(int)i*20, 500 - (int)i*5, 0.3 - i/99, (45+i*.2)*Math.PI/180);
            }
            
            return Source;
        }

        private void DrawStamp(int x, int y, double scale, double angle)
        {
            var a = angle + 1.5708;//plus 90deg, which is neutral
            var up = new Vec2(Math.Cos(a), Math.Sin(a));
            up.Normalize();
            var right = up.Perpendicular();
            
            var stampWidth = Math.Abs(MyStamp.UpperLeft.X - MyStamp.Center.X) * 2;
            var stampHeight = Math.Abs(MyStamp.UpperLeft.Y - MyStamp.Center.Y) * 2;
            
            var ht = stampHeight / 2.0;
            var wd = stampWidth / 2.0;
            var htVector = new Vec2((int)Math.Round(-ht * up.dX), (int)Math.Round(-ht * up.dY));
            var wdVector = new Vec2((int)Math.Round(-wd * right.dX), (int)Math.Round(-wd * right.dY));
            var vec = htVector + wdVector;
            Vec2 centerOfStampOnImage = new Vec2(x, y);
            var rotationPoint = centerOfStampOnImage + vec;
            var xStart = rotationPoint.X;
            var yStart = rotationPoint.Y;
            
            for (double yInc = 0; yInc < stampHeight * scale; yInc++)
            {
                var yPos = yInc * up.dY + yStart;
                var xPos = yInc * up.dX + xStart;

                var rowStartPosition = new Vec2((int)Math.Round(xPos), (int)Math.Round(yPos));
                DrawRowOfStamp(rowStartPosition, right, up, MyStamp.Width, yInc, scale);
            }
        }

        private void DrawRowOfStamp(Vec2 startPosition, Vec2 right, Vec2 up, int stampWidth, double upIncrements, double scale)
        {
            var rightmostX = Math.Round(stampWidth * right.dX);
            var stampHeight = Math.Abs(MyStamp.UpperLeft.Y - MyStamp.Center.Y) * 2;
            
            for (double xInc = 0; xInc < stampWidth * scale; xInc++)
            {
                var xPos = startPosition.X + right.dX * xInc;
                var yPos = startPosition.Y + right.dY * xInc;

                var gridPositions = GetRoundedPositions(xPos, yPos);
                var stampX = MyStamp.UpperLeft.X + xInc/scale;
                var stampY = MyStamp.UpperLeft.Y + upIncrements/scale;
                var stampPositions = GetRoundedPositions(stampX, stampY);
                var stampPosition = new Vec2(0, 0);
                if (stampPositions.Length == 1)
                    stampPosition = stampPositions.First();
                else
                {
                    var dist = double.MaxValue;
                    foreach (var pos in stampPositions)
                    {
                        var pDist = Vec2.Distance(stampX, stampY, pos);
                        if (pDist < dist)
                        {
                            stampPosition = pos;
                            dist = pDist;
                        }
                    }
                }
              
                var stampColor = MyStamp.ClosestColor(stampPosition);
                foreach (var pos in gridPositions)
                {
                    if (pos.Y < 0 || pos.Y > Source.Header.Height-1)
                        continue;
                    else if (pos.X < 0 || pos.X > Source.Header.Width-1)
                        continue;
                    
                    var gridColor = Source.Grid[pos.Y].Pixels[pos.X];

                    //Simply set it to the color
                    gridColor.R = stampColor.R;
                    gridColor.G = stampColor.G;
                    gridColor.B = stampColor.B;

                    //Blend it with the color
                    //  BlendColor(stampColor, gridColor, stampPosition, MyStamp);
                }
            }
        }
    }
}
