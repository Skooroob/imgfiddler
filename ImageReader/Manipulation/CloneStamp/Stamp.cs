﻿using ImageReader.Mafs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.CloneStamp
{
    public class Stamp
    {

        public Dictionary<Vec2, Color> Pixels { get; set; }
        public Vec2 Center { get; set; }
        public Color AverageColor { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Vec2 UpperLeft { get; set; }
        public double StampRadius { get; set; }

        public Stamp()
        {
            Pixels = new Dictionary<Vec2, Color>();
        }

        public void GetAvgColor()
        {
            var rav = 0;
            var gav = 0;
            var bav = 0;
            var aav = 0;
            foreach (var kpv in Pixels)
            {
                rav += kpv.Value.R;
                gav += kpv.Value.G;
                bav += kpv.Value.B;
                aav += kpv.Value.A;
            }
            var R = (byte)Math.Round((double)rav / Pixels.Count);
            var G = (byte)Math.Round((double)gav / Pixels.Count);
            var B = (byte)Math.Round((double)bav / Pixels.Count);
            var A = (byte)Math.Round((double)aav / Pixels.Count);
            AverageColor = new Color(R, G, B, A);
        }

        public void CalcluateCenter()
        {
            var upperLeft = new Vec2(int.MaxValue, 0);
            var left = new Vec2((int)int.MaxValue, 0);
            var right = new Vec2();
            var up = new Vec2();
            var down = new Vec2(0, (int)int.MaxValue);
            foreach (var kpv in Pixels)
            {
                var position = kpv.Key;
                if (position.X < left.X)
                    left = position;
                if (position.X > right.X)
                    right = position;
                if (position.Y > up.Y)
                    up = position;
                if (position.Y < down.Y)
                    down = position;

                if (position.X < upperLeft.X && position.Y > upperLeft.Y)
                    upperLeft = position;
            }

            var midX = left.X + Math.Abs(left.X - right.X);
            var midY = down.Y + Math.Abs(down.Y - up.Y);
            Center = Closest(new Vec2(midX, midY));

            UpperLeft = upperLeft;
            StampRadius = Vec2.Distance(UpperLeft, Center)*0.8f;
            Width = Math.Abs((UpperLeft.X - Center.X) * 2);
            Height = Math.Abs((UpperLeft.Y - Center.Y) * 2);
        }

        public Vec2 Closest(Vec2 position)
        {
            if (Pixels.ContainsKey(position))
                return position;


            var dist = double.MaxValue;
            Vec2 current = new Vec2();
            foreach (var kpv in Pixels)
            {
                var pos = kpv.Key;
                var pDist = Vec2.Distance(position, pos);
                if (pDist < dist)
                {
                    current = pos;
                    dist = pDist;
                }
            }
            return current;
        }

        public Color ClosestColor(Vec2 position)
        {
            if (Pixels.ContainsKey(position))
                return Pixels[position];
            
            var dist = double.MaxValue;
            Color current = new Color();
            foreach (var kpv in Pixels)
            {
                var pos = kpv.Key;
                var pDist = Vec2.Distance(position, pos);
                if (pDist < dist)
                {
                    current = kpv.Value;
                    dist = pDist;
                }
            }
            return current;
        }

    }
}
