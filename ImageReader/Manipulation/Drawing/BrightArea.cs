﻿using ImageReader.Mafs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.Drawing
{
    public class BrightArea
    {
        public Color MinBrightness { get; set; }
        public Dictionary<Vec2, Color> Pixels { get; set; }
        public Vec2 Center { get; set; }
        public Dictionary<Vec2, float> Rays { get; set; }
        public BrightArea()
        {
            Pixels = new Dictionary<Vec2, Color>();
            Rays = new Dictionary<Vec2, float>();
        }

        public void CalculateCenter()
        {
            var xSum = 0;
            var ySum = 0;
            foreach (var item in Pixels)
            {
                var pos = item.Key;
                xSum += pos.X;
                ySum += pos.Y;
            }
            var xAvg = (int)((float)xSum / (float)Pixels.Count);
            var yAvg = (int)((float)ySum / (float)Pixels.Count);
            Center = new Vec2(xAvg, yAvg);
        }
    }
}
