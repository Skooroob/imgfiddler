﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ImageReader.Mafs;
using ImageReader.Png;

namespace ImageReader.Manipulation.Drawing
{
    /// <summary>
    /// Draws a line from left to right with a given slope until the edge of the image is reached
    /// </summary>
    public class RaysOfLight : IManipulator
    {
        //Max: 1228
        //Avg: 112
        public float Dampening = 1.0f;
        public int _maxBrightAreaSize = 1300;
        private int _minBrightAreaSize = 3;
        public int _minBrightDistance = 16;//7
        public int MaxRayLength = 688;//320, 336
        private int _radius = 60;
        PngImage _source;
        Vec2 _pos = new Vec2();
        public Color MinBrightness = new Color(220, 220, 220);
        private List<BrightArea> BrightAreas;

        public PngImage Run(PngImage source)
        {
            _source = source;
            //1. find a bright area. put the pixels in a BrightArea object.
            //Don't add pixels to more than one brightArea object
            GetBrightAreas();
            SortBrightAreas();
            //DebugBrightAreas();
            //2. foreach distinct BrightArea
            //draw rays of light from the center of the bright area going out a distance porportional to the
            //bright area's size
            DefineRays();
            // DrawRays();
            DrawFans();
            return _source;
        }

        private void DrawRays()
        {

            foreach (var brightArea in BrightAreas)
            {
                var center = brightArea.Center;
                foreach (var ray in brightArea.Rays)
                {
                    //draw a line from brightArea.Center in the direction of ray.Key for ray.value units
                    var rayLen = ray.Value;
                    var direction = ray.Key;
                    var currentPosition = center;
                   // var ballRadius = 1;
                 //  var exclude = new List<Pixel>();

                    //now what we want is to slowly rotate to the left (and draw the ray for each rotation)
                    //then do the same to the right, resulting in a fan of light emanating from the center

                    for (int i = 0; i < ray.Value; i++)
                    {
                        var width = (int)((i + (ray.Value / 18)) / ((i * i) + 1));
                        if (width == 0)
                            width = 1;
                        //we draw a very small circle at this point (white)
                        //and increment in the direction of the ray
                        var dx = (ray.Key.dX * i) + center.X;
                        var dy = (ray.Key.dY * i) + center.Y;
                        
                        var adj = AdjacentPixels(dx, dy, width);
                        var colorDampen = ((ray.Value - (float)i) / ray.Value);
                        foreach (var px in adj)
                        {
                            var rDist = 255 - px.R;
                            var rAdd = (int)(colorDampen * rDist);
                            px.R = (byte)(px.R + rAdd);

                            var gDist = 255 - px.G;
                            var gAdd = (int)(colorDampen * gDist);
                            px.G = (byte)(px.G + gAdd);

                            var bDist = 255 - px.B;
                            var bAdd = (int)(colorDampen * bDist);
                            px.B = (byte)(px.B + bAdd);
                        }

                        //exclude = circlePixels;
                    }
                }
            }
        }

        private void DrawFans()
        {
            float maxLen = 0;

            foreach (var brightArea in BrightAreas)
            {
                var center = brightArea.Center;
                foreach (var ray in brightArea.Rays)
                {
                    var rayLen = ray.Value;
                    if (rayLen > maxLen)
                        maxLen = rayLen;
                }
            }

                    const double deg = 0.0174533;
            foreach (var brightArea in BrightAreas)
            {
                var center = brightArea.Center;
                foreach (var ray in brightArea.Rays)
                {
                    //draw a line from brightArea.Center in the direction of ray.Key for ray.value units
                    var rayLen = ray.Value;
                    var direction = ray.Key;
                    var currentPosition = center;



                    //the most ever was about 340
                    //var srProp = rayLen / 340.0;
                    var srProp = rayLen / maxLen;
                    var amtOfSubRays = (int)Math.Round(srProp * 24.0);
                    if (amtOfSubRays % 2 != 0)
                        amtOfSubRays++;
                    // var ballRadius = 1;
                    //  var exclude = new List<Pixel>();

                    var rayWidth = 1;
                    //var rayWidth = (int)Math.Round(srProp * 2.0);//some function of max ray len
                    //the denominator must also be a function of the ray length
                    double denominator = srProp * 9;//5=pretty
                    double angleIncrement = (deg / denominator);//formerly 3.0

                    double dampeningOffset = 1.0 - srProp;//0.2 for the smallest of rays
                    if (dampeningOffset > 0.4)
                        dampeningOffset = 0.4;

                    //now what we want is to slowly rotate to the left (and draw the ray for each rotation)
                    //then do the same to the right, resulting in a fan of light emanating from the center

                    for (int i = 0; i < ray.Value; i++)
                    {
                        var initialDirectionX = ray.Key.dX;
                        var initialDirectionY = ray.Key.dY;

                        //var width = (int)((i + (ray.Value / 18)) / ((i * i) + 1));
                        //if (width == 0)
                        //    width = 1;


                        var initialRotation = Vec2.GetAngle(ray.Key, new Vec2((double)1, (double)0));
                        initialRotation *= -1;
                        var initialDegrees = initialRotation * 180.0f;
                        initialDegrees /= Math.PI;
                        //the denominator must also be a function of the ray length
                       // double angleIncrement = (deg / 3.0);

                       // var amt = 12 * ray.Value;///some function of how long the ray is

                        //try drawing three rays, each rotating a little more from the initialDirection
                        bool otherSide = false;
                        int halfway = amtOfSubRays / 2;
                        halfway--;
                        for (int k = 0; k < amtOfSubRays; k++)
                        {
                            if (k == halfway)
                                otherSide = true;
                            double nextAngle = 0;

                            //first do no rotations (because k = 0)
                            if (!otherSide)
                                nextAngle = initialRotation + (k * angleIncrement);
                            else
                                nextAngle = initialRotation + ((halfway - k) * angleIncrement);

                            var nX = Math.Cos(nextAngle);//these use radians. deg is human-only
                            var nY = Math.Sin(nextAngle);
                            //now use nX, nY as the direction in which to draw the ray

                            var dx = (nX * i) + center.X;
                            var dy = (nY * i) + center.Y;

                            var adj = AdjacentPixels(dx, dy, rayWidth);
                            var colorDampen = ((ray.Value - (float)i) / ray.Value)- dampeningOffset;
                            if (colorDampen <= 0)
                                break;

                            foreach (var px in adj)
                            {
                                var rDist = 255 - px.R;
                                var rAdd = (int)(colorDampen * rDist);
                                px.R = (byte)(px.R + rAdd);

                                var gDist = 255 - px.G;
                                var gAdd = (int)(colorDampen * gDist);
                                px.G = (byte)(px.G + gAdd);

                                var bDist = 255 - px.B;
                                var bAdd = (int)(colorDampen * bDist);
                                px.B = (byte)(px.B + bAdd);
                            }
                        }
                        
                    }
                }
            }
        }
        
        private void DefineRays()
        {
            //So { x, y } becomes { y, -x }.
           
            //var down = new Vec2(0, -1);
            //var up = new Vec2(0, 1);
            //var left = new Vec2(-1, 0);
            //var right = new Vec2(1, 0);
            
            for (int i = 0; i < BrightAreas.Count; i++)
            {
                var bright = BrightAreas[i];
                //how far out shall the ray go?
                var amount = bright.Pixels.Count;
                double proportion = (float)amount / (float)_maxBrightAreaSize;
                var min = (int)(proportion * 5);
                //if (min == 0)
                //    min = 0;
                var max = (int)(proportion * 23);//6
                if (min >= max)
                {
                    min = 1;
                    max++;
                }
                var rrand = new Random();
                var rayCasts = rrand.Next(2 + min, 5 + max);//3+max
                while (rayCasts % 2 != 0)
                    rayCasts++;

                for (int k = 0; k < rayCasts; k++)
                {
                    Thread.Sleep(rrand.Next(10, 22));
                    var rand = new Random();
                    var rX = rand.NextDouble();
                    var neg = rand.NextDouble();
                    if (neg > 0.5f)
                        rX *= -1;
                    var rY = Math.Sqrt(1 - Math.Pow(rX, 2.0f));
                    var direction = new Vec2(rX, rY);
                    var oppositeDirection = direction.OppositeDirection();
                    var perpDirection = direction.Perpendicular();
                    var oppPerpDirection = perpDirection.OppositeDirection();

                    var lenFactor = rand.NextDouble();
                    if (neg > 0.5 && proportion > 0.8f)
                        lenFactor += 1;

                    var raylen = (float)((float)(proportion * MaxRayLength) * lenFactor) * Dampening;
                    //we need this to be inversely proportionate
                    if (!bright.Rays.ContainsKey(direction))
                    {
                        bright.Rays.Add(direction, raylen);
                        bright.Rays.Add(oppositeDirection, raylen);
                        bright.Rays.Add(perpDirection, raylen);
                        bright.Rays.Add(oppPerpDirection, raylen);
                    }
                }
            }
        }

        private void DebugBrightAreas()
        {
            _radius = 22;
            foreach (var brightArea in BrightAreas)
            {
                _pos = brightArea.Center;
                var adj = AdjacentPixels(_pos, _radius);
                foreach (var px in adj)
                {
                    //set the color to brutal red
                    px.R = 255;
                    px.G = 0;
                    px.B = 0;
                }
            }
        }

        private void SortBrightAreas()
        {
            for (int i = 0; i < BrightAreas.Count; i++)
            {
                var brightArea = BrightAreas[i];
                if (brightArea.Pixels.Count > _maxBrightAreaSize 
                    || brightArea.Pixels.Count < _minBrightAreaSize)//6000
                {
                    BrightAreas.RemoveAt(i);
                    i--;
                    continue;
                }
                // brightArea.CalculateCenter();
                //prune bright areas that are too close to one another
                for (int k = 0; k < BrightAreas.Count; k++)
                {
                    if (k == i)
                        continue;

                    var dist = Vec2.Distance(BrightAreas[i].Center, BrightAreas[k].Center);
                    if (dist < _minBrightDistance)
                    {

                        BrightAreas.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
        }
        
        private void GetBrightAreas()
        {
            var brushRadius = 2;//12
            var brightAreas = new List<BrightArea>();
            var currentBrightArea = new BrightArea();
            var found = false;
            var haveEverFound = false;
            Vec2 lastPosition = new Vec2(0, 0);
            for (int y = brushRadius; y < _source.Grid.Count; y+=brushRadius)
            {
                for (int x = brushRadius; x < _source.Grid[y].Pixels.Count; x+=brushRadius)
                {
                    var position = new Vec2(x, y);
                    var color = _source.Grid[y].Pixels[x];

                    //rather than one color, lets increment by brush radius and sample therefrom
                    //var colors = AdjacentPixels(position, brushRadius);
                    //foreach (var color in colors)
                    //{

                    //}

                    if (!MinBrightness.IsBrighterThan(color))
                    {
                        //if it's not already in a bright area....
                        
                        if (!found)
                        {
                            //this is the first px of a new bright area
                            if (haveEverFound)
                            {
                                //what is the distance between this new bright area and the last one?
                                var dist = Vec2.Distance(lastPosition, position);
                                //if less than 4 px, discard it.
                                if (dist < _minBrightDistance)
                                    continue;
                            }
                        }

                        if (NotInAnyBrigthArea(position, brightAreas))
                        {
                            currentBrightArea.Pixels.Add(position, color);
                            found = true;
                            haveEverFound = true;
                        }
                    }
                    else if (found)
                    {
                        found = false;
                        currentBrightArea.CalculateCenter();
                        lastPosition = currentBrightArea.Center;
                        brightAreas.Add(currentBrightArea);
                        currentBrightArea = new BrightArea();
                    }
                }
            }
            if (found)
            {
                currentBrightArea.CalculateCenter();
                brightAreas.Add(currentBrightArea);
            }
            BrightAreas = brightAreas;
        }

        public bool NotInAnyBrigthArea(Vec2 position, List<BrightArea> areas)
        {
            foreach (var area in areas)
                if (area.Pixels.ContainsKey(position))
                    return false;
            return true;
        }
        
        /// <summary>
        /// TODO: Accept normalized vector and length. Consider making it part of a lib for re-use
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private List<Color> AdjacentPixels(Vec2 pos, int radius)
        {
            var x = pos.X;
            var y = pos.Y;

            //only search a square around the circle
            var xStart = x - radius;
            if (xStart < 0)
                xStart = 0;
            var xEnd = x + radius;
            if (xEnd > _source.Header.Width)
                xEnd = _source.Header.Width;
            var yStart = y - radius;
            if (yStart < 0)
                yStart = 0;
            var yEnd = y + radius;
            if(yEnd > _source.Header.Height)
                yEnd = _source.Header.Height;
            
            var result = new ConcurrentBag<Color>();
            Parallel.For(yStart, yEnd, y2 =>
            {
                var row = _source.Grid[y2];
                for (int x2 = xStart; x2 < xEnd; x2++)
                {
                    var distance = Vec2.Distance(pos, new Vec2(x2, y2));
                    if (distance <= radius)
                        result.Add(row.Pixels[x2]);
                }
            });


            //for (int y2 = yStart; y2 < yEnd; y2++)
            //{
            //    var row = _source.Grid[y2];
            //    for (int x2 = xStart; x2 < xEnd; x2++)
            //    {
            //        //var lef = ((float)x - (float)x2);
            //        //lef *= lef;
            //        //var rig = ((float)y - (float)y2);
            //        //rig *= rig;
            //        //var distance = Math.Sqrt(lef + rig);
            //        var distance = Vec2.Distance(pos, new Vec2(x2, y2));
            //       // Console.WriteLine($"Position: {x},{y}. OtherPos: {x2}, {y2}. Distance: {distance}");
            //        if (distance <= radius)
            //            result.Add(row.Pixels[x2]);
            //    }
            //}


            return result.ToList();
        }

        private List<Color> AdjacentPixels(double x, double y, int radius)
        {
            //only search a square around the circle
            var xStart = (int)(x - radius);
            if (xStart < 0)
                xStart = 0;
            var xEnd = (int)(x + radius);
            if (xEnd > _source.Header.Width)
                xEnd = _source.Header.Width;
            var yStart = (int)(y - radius);
            if (yStart < 0)
                yStart = 0;
            var yEnd = (int)(y + radius);
            if (yEnd > _source.Header.Height)
                yEnd = _source.Header.Height;

            var result = new ConcurrentBag<Color>();
            Parallel.For(yStart, yEnd, y2 =>
            {
                var row = _source.Grid[y2];
                for (int x2 = xStart; x2 < xEnd; x2++)
                {
                    var distance = Vec2.Distance(x, y, new Vec2(x2, y2));
                    if (distance <= radius)
                        result.Add(row.Pixels[x2]);
                }
            });
            return result.ToList();
        }
        
    }
}
