﻿using ImageReader.Mafs;
using ImageReader.Manipulation.CloneStamp;
using ImageReader.Png;
using ImageReader.Roles;
using ImageReader.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.FullProgram
{
    public class AkiraGrandma : IManipulator
    {
        public PngImage Image { get; set; }
        public PngImage Sample { get; set; }

        private Stamp MyStamp = new Stamp();

        private Box smallImage  = new Box(325, 357, 173, 213);
        private Box largeImage = new Box(3259, 3559, 1709, 2109);

        public PngImage Run(PngImage source)
        {
            Image = source;


            //GetSample();
            //GetStampFromSample(largeImage);

            var blender = new BlendFavoritArea();
            blender.minX = smallImage.minX;
            blender.maxX = smallImage.maxX;
            blender.minY = smallImage.minY;
            blender.maxY = smallImage.maxY;
            blender.Passes = 3;
            blender.Feedback = true;
            blender.InitiatingColorDistance = 60;
            blender.ColorDistanceDropoff = 63;
            blender.MaxScale = 6;
            blender.MinDist = 12;
            var img = blender.Run(Image);


            //var blender = new BlendFavoritArea(MyStamp);

            //blender.Passes = 2;
            //blender.Feedback = true;
            //blender.MaxScale = 4;
            //blender.InitiatingColorDistance = 60;
            //blender.ColorDistanceDropoff = 66;
            //blender.MinDist = 12;
            //var img = blender.Run(Image);

            return Image;
        }

        private void GetSample()
        {
            var getter = new UseSchema("testSettings2.json");
            var data = getter.GetData();
            var loader = New.ImageLoader();
            Sample = loader.Load(data);
        }

        private void GetStampFromSample(Box box)
        {
            for (int y = box.minY; y < box.maxY && y < Sample.Grid.Count; y++)
            {
                var row = Sample.Grid[y];
                for (int x = box.minX; x < box.maxX; x++)
                {
                    var position = new Vec2(x, y);
                    var color = Sample.Grid[y].Pixels[x];
                    MyStamp.Pixels.Add(position, color);
                }
            }
            MyStamp.GetAvgColor();
            MyStamp.CalcluateCenter();
        }

    }
}
