﻿using ImageReader.Mafs;
using ImageReader.Manipulation.CloneStamp;
using ImageReader.Png;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.FullPrograms
{
    public class AnyPicture
    {
        private Box _box = new Box(328, 465, 165, 318);

        public PngImage Run(PngImage source)
        {
            var rs = new RotationsAndScales(_box);
            rs.DropWhite = true;
            var result = rs.Run(source);


            return result;
        }
    }
}
