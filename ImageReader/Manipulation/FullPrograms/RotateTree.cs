﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Mafs;
using ImageReader.Manipulation.CloneStamp;
using ImageReader.Png;

namespace ImageReader.Manipulation.FullPrograms
{
    public class RotateTree : IManipulator
    {
        //private Box _box = new Box(280, 315, 225, 270);
        private Box _box = new Box(330, 370, 177, 215);

        public PngImage Run(PngImage source)
        {

           // var m = new MarkImage(_box);
           //var result = m.Run(source);

            var rs = new RotationsAndScales(_box);
            rs.DropWhite = true;
            var result = rs.Run(source);

            //center should be
            //350, 196



            //42 , 42

            return result;
        }
    }
}
