﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Manipulation.BlurEffect;
using ImageReader.Manipulation.CloneStamp;
using ImageReader.Manipulation.Drawing;
using ImageReader.Png;

namespace ImageReader.Manipulation.FullPrograms
{
    public class StarsAndSuch : IManipulator
    {
        public PngImage Image { get; set; }
        public PngImage Run(PngImage source)
        {
            Image = source;

            var blender = new BlendFavoritArea();
            blender.minX = 740;
            blender.maxX = 780;
            blender.minY = 24;
            blender.maxY = 86;
            blender.Passes = 3;
            blender.Feedback = true;
            blender.MaxScale = 6;
            blender.InitiatingColorDistance = 50;
            blender.ColorDistanceDropoff = 55;
            Image = blender.Run(Image);

            blender = new BlendFavoritArea();
            blender.minX = 360;
            blender.maxX = 420;
            blender.minY = 214;
            blender.maxY = 266;
            blender.Passes = 2;
            blender.Feedback = true;
            blender.MaxScale = 2;
            blender.InitiatingColorDistance = 50;
            blender.ColorDistanceDropoff = 55;
            blender.MinDist = 16;
            Image = blender.Run(Image);


            var d = new RaysOfLight();
            d._maxBrightAreaSize = 10;
            d._minBrightDistance = 24;
            d.MinBrightness = new Color(210,210,210);
            d.Dampening = 0.5f;
            Image = d.Run(Image);
            Image = d.Run(Image);
            Image = d.Run(Image);
            Image = d.Run(Image);

            var set = NeatBlurPresets.Subtle();
            set.Passes = 1;
            set.ContrastCutoff = 60;
            set.Invert = false;
            var b2 = new Blur(set);
            Image = b2.Run(Image);

            blender = new BlendFavoritArea();
            blender.minX = 24;
            blender.maxX = 59;
            blender.minY = 621;
            blender.maxY = 644;
            blender.Passes = 1;
            blender.Feedback = true;
            blender.MaxScale = 2;
            blender.InitiatingColorDistance = 50;
            blender.ColorDistanceDropoff = 55;
            blender.MinDist = 16;
            Image = blender.Run(Image);

            
            d.Dampening = 0.333f;
            Image = d.Run(Image);
            Image = d.Run(Image);
            Image = d.Run(Image);


            blender = new BlendFavoritArea();
            blender.minX = 24;
            blender.maxX = 59;
            blender.minY = 621;
            blender.maxY = 644;
            blender.Passes = 2;
            blender.Feedback = true;
            blender.MaxScale = 2;
            blender.InitiatingColorDistance = 50;
            blender.ColorDistanceDropoff = 55;
            blender.MinDist = 16;
            Image = blender.Run(Image);
            
            d.Dampening = 0.133f;
            Image = d.Run(Image);
            Image = d.Run(Image);
            Image = d.Run(Image);
            Image = d.Run(Image);



            var ecks = new Blur(NeatBlurPresets.AccentuateExistingTiltShift());
            Image = ecks.Run(Image);

            return Image;
        }
    }
}
