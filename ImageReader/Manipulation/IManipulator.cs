﻿using ImageReader.Png;

namespace ImageReader.Manipulation
{
    public interface IManipulator
    {
        PngImage Run(PngImage source);
    }
}
