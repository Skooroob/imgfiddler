﻿using ImageReader.Mafs;
using ImageReader.Manipulation.Drawing;
using ImageReader.Png;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImageReader.Manipulation.RedWaveEffect
{
    public class RedWave
    {

        public int Passes = 3;
        public bool Feedback { get; set; }
        private Color _pureRed = new Color(255, 0, 0);
        private Color _reddest;
        private Color _targetHighestRedness;
        private float _reddestDistance;
        //max number in color distance to be considered red. The lower the number, the less red streamers appear
        public float RedThreshold = 16;
        public int TraversalRadius= 5;

        private int _minRedDistance = 6;//12
        PngImage _source;
        Vec2 _pos = new Vec2();
        private Color _minRedness = new Color(220, 20, 20);
        //private List<BrightArea> BrightAreas;

        public PngImage Run(PngImage source)
        {
            if (Feedback)
                Console.WriteLine("Redwave");
            for (int i = 0; i < Passes; i++)
            {
                if(Feedback)
                    Console.WriteLine($"Pass: {i+1}/{Passes}");
                _source = source;
                GetReddestColor();
                GetRedAreas();
            }
            return _source;
        }

        private void GetReddestColor()
        {
            _reddest = new Color(0, 255, 255);
            _reddestDistance = _reddest.ColorDistance(_pureRed);
            Parallel.For(0, _source.Grid.Count, index =>
            {
                var row = _source.Grid[index];
                for (int x = 0; x < row.Pixels.Count; x++)
                {
                    var color = row.Pixels[x];
                    var distToRed = color.ColorDistance(_pureRed);
                    if (distToRed < _reddestDistance)
                    {
                        _reddest = color;
                        _reddestDistance = distToRed;
                    }
                }
            });

            //reddest color to be found: 130, 128, 128
            //purRed distance of 159
            //this is now the metric by which we will grab the reddest pixels from the image
            var bumpR = _reddest.R + 20;
            if (bumpR > 255)
                bumpR = 255;
            var decG = _reddest.G - 20;
            if (decG < 0)
                decG = 0;
            var decB = _reddest.B - 20;
            if (decB < 0)
                decB = 0;
            _targetHighestRedness = new Color((byte)bumpR, (byte)decG, (byte)decB);

        }
        
        private bool CloseToUsedPositions(List<Vec2> used, Vec2 position)
        {
            foreach (var uPos in used)
            {
                var dist = Vec2.Distance(position, uPos);
                if (dist < _minRedDistance)
                    return true;
            }
            return false;
        }

        private void GetRedAreas()
        {

            var centerY = _source.Header.Height / 2;
            var centerX = _source.Header.Width / 2;
            var imageCenter = new Vec2(centerX, centerY);
            //the farthest possible distance from the center?
            var farthestPossibleDistanceFromCenter = Vec2.Distance(new Vec2(0, 0), imageCenter);

            var usedPositions = new List<Vec2>();

            //var TraversalRadius = 5;//7
            var redAreas = new List<BrightArea>();
            var currentRedArea = new BrightArea();
            var haveEverFound = false;
            Vec2 lastPosition = new Vec2(0, 0);
            for (int y = TraversalRadius; y < _source.Grid.Count; y += TraversalRadius)
            {
                if (Feedback)
                    Console.WriteLine($"{y}/{_source.Grid.Count}");

                for (int x = TraversalRadius; x < _source.Grid[y].Pixels.Count; x += TraversalRadius)
                {
                    var position = new Vec2(x, y);
                    var color = _source.Grid[y].Pixels[x];

                    //rather than one color, lets increment by brush radius and sample therefrom
                    var colors = AdjacentPixels(position, TraversalRadius);
                    foreach (var c in colors)
                    {
                        //if it's red enough put it in a red area
                        //for now, just enhance the redness in order to debug
                        var redDist = c.ColorDistance(_pureRed);
                        if (Math.Abs(redDist - _reddestDistance) < RedThreshold)
                        {
                            if(haveEverFound)
                            {
                                //var dist = Vec2.Distance(position, lastPosition);
                                //if (dist < _minRedDistance)
                                //    continue;
                                if (CloseToUsedPositions(usedPositions, position))
                                    continue;
                            }


                            //make it redder by a factor of its distance from the reddest
                            var rProp = redDist / _reddestDistance;

                            var toCenter = new Vec2(imageCenter.X - position.X, imageCenter.Y - position.Y);
                            //now we need to normalize... but how?!!!
                            toCenter.AsDirection();
                            toCenter.Normalize();


                            //u know what, lets just draw the wave here
                           // var direction = new Vec2((double)0, (double)-1.0);
                            var direction = new Vec2((double)0, (double)-1.0);
                            direction.Normalize();
                           // var direction = toCenter;
                            var horizontal = direction.Perpendicular();
                            var negHorizontal = horizontal.OppositeDirection();
                            var radius = 1;
                            
                            var amplitude = rProp * 2.0;

                            var distanceFromCenter = Vec2.Distance(position, imageCenter);
                            var distanceProportion = distanceFromCenter / farthestPossibleDistanceFromCenter;
                            //the farthest possible distance from the center?
                            //frequence should be a function of distance from the center of the image
                            var freq = distanceProportion * 6f;
                            var vectorLen = (distanceProportion * 333);
                            // var opacity = distanceProportion * 0.3;
                            var horizontalShadingCount = distanceProportion * 22.0;
                            for (int r = 0; r < vectorLen; r++)
                            {
                                var opFac = 0.0335 * distanceProportion * Math.Sin(Math.PI * ((double)r / vectorLen));

                                //the y changes incrementally, so our x becomes a function of y
                                //(for a sin wave along the vector 'up')
                                //How can we figure out our amplitude?
                                //

                                //we now have an x and y offset because we're not going straight up
                                var horizontalOffset = amplitude * Math.Sin((double)r/freq);
                                //var hdx = horizontal.dX * r * horizontalOffset;
                                //var hdy = horizontal.dY * r * horizontalOffset;
                                
                                var dx = (direction.dX * r) + position.X + horizontalOffset;
                                var dy = (direction.dY* r) + position.Y;
                                var adj = AdjacentPixels(dx, dy, radius);
                                
                                foreach (var px in adj)
                                {
                                    //get distances necessary to raise it to target redness
                                    var rDist = _targetHighestRedness.R - px.R;
                                    var gDist = _targetHighestRedness.G - px.G;
                                    var bDist = _targetHighestRedness.B - px.B;

                                    //add colors
                                    var rAdd = rProp * (double)rDist * opFac;
                                    var gAdd = rProp * (double)gDist * opFac;
                                    var bAdd = rProp * (double)bDist * opFac;

                                    px.R = (byte)(px.R + ((int)Math.Round(rAdd)));
                                    px.G = (byte)(px.G + ((int)Math.Round(gAdd)));
                                    px.B = (byte)(px.B + ((int)Math.Round(bAdd)));

                                    //now draw a strip going to the right, starting from this opacity, until totally transparent
                                    //but not for more than 8 px
                                    //we could blend these toward the center of the screen if desired
                                    var horizOpac = opFac;
                                    var horizBlendFac = 0.03;
                                    for (int shadeIndex = 1; shadeIndex < horizontalShadingCount; shadeIndex++)
                                    {
                                        horizOpac -= horizBlendFac;
                                        if (horizOpac < 0)
                                            break;

                                        var rightBlend = AdjacentPixels(dx + shadeIndex, dy, 1);
                                        foreach (var rpx in rightBlend)
                                        {
                                            //get distances necessary to raise it to target redness
                                            var hrDist = _targetHighestRedness.R - rpx.R;
                                            var hgDist = _targetHighestRedness.G - rpx.G;
                                            var hbDist = _targetHighestRedness.B - rpx.B;

                                            //add colors
                                            var hrAdd = rProp * (double)rDist * horizOpac;
                                            var hgAdd = rProp * (double)gDist * horizOpac;
                                            var hbAdd = rProp * (double)bDist * horizOpac;

                                            rpx.R = (byte)(rpx.R + ((int)Math.Round(hrAdd)));
                                            rpx.G = (byte)(rpx.G + ((int)Math.Round(hgAdd)));
                                            rpx.B = (byte)(rpx.B + ((int)Math.Round(hbAdd)));
                                        }
                                    }
                                }

                            }


                            lastPosition = new Vec2(x, y);
                            usedPositions.Add(lastPosition);

                            haveEverFound = true;
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// TODO: Accept normalized vector and length. Consider making it part of a lib for re-use
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private List<Color> AdjacentPixels(Vec2 pos, int radius)
        {
            var x = pos.X;
            var y = pos.Y;

            //only search a square around the circle
            var xStart = x - radius;
            if (xStart < 0)
                xStart = 0;
            var xEnd = x + radius;
            if (xEnd > _source.Header.Width)
                xEnd = _source.Header.Width;
            var yStart = y - radius;
            if (yStart < 0)
                yStart = 0;
            var yEnd = y + radius;
            if (yEnd > _source.Header.Height)
                yEnd = _source.Header.Height;

            var result = new ConcurrentBag<Color>();
            Parallel.For(yStart, yEnd, y2 =>
            {
                var row = _source.Grid[y2];
                for (int x2 = xStart; x2 < xEnd; x2++)
                {
                    var distance = Vec2.Distance(pos, new Vec2(x2, y2));
                    if (distance <= radius)
                        result.Add(row.Pixels[x2]);
                }
            });


            //for (int y2 = yStart; y2 < yEnd; y2++)
            //{
            //    var row = _source.Grid[y2];
            //    for (int x2 = xStart; x2 < xEnd; x2++)
            //    {
            //        //var lef = ((float)x - (float)x2);
            //        //lef *= lef;
            //        //var rig = ((float)y - (float)y2);
            //        //rig *= rig;
            //        //var distance = Math.Sqrt(lef + rig);
            //        var distance = Vec2.Distance(pos, new Vec2(x2, y2));
            //       // Console.WriteLine($"Position: {x},{y}. OtherPos: {x2}, {y2}. Distance: {distance}");
            //        if (distance <= radius)
            //            result.Add(row.Pixels[x2]);
            //    }
            //}


            return result.ToList();
        }

        private List<Color> AdjacentPixels(double x, double y, int radius)
        {
            //only search a square around the circle
            var xStart = (int)(x - radius);
            if (xStart < 0)
                xStart = 0;
            var xEnd = (int)(x + radius);
            if (xEnd > _source.Header.Width)
                xEnd = _source.Header.Width;
            var yStart = (int)(y - radius);
            if (yStart < 0)
                yStart = 0;
            var yEnd = (int)(y + radius);
            if (yEnd > _source.Header.Height)
                yEnd = _source.Header.Height;

            var result = new ConcurrentBag<Color>();
            Parallel.For(yStart, yEnd, y2 =>
            {
                var row = _source.Grid[y2];
                for (int x2 = xStart; x2 < xEnd; x2++)
                {
                    var distance = Vec2.Distance(x, y, new Vec2(x2, y2));
                    if (distance <= radius)
                        result.Add(row.Pixels[x2]);
                }
            });
            return result.ToList();
        }

    }
}
