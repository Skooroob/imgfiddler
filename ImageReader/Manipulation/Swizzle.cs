﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageReader.Png;

namespace ImageReader.Manipulation
{
    public class Swizzle : IManipulator
    {
        private static Color Red = new Color(255, 0, 0);


        public PngImage Run(PngImage source)
        {
            for (int i = 0; i < source.Grid.Count; i++)
                Swiz(source.Grid[i]);
            return source;
        }

        private void Swiz(PngRow row)
        {
            for (int i = 0; i < row.Count; i++)
            {
                var px = row.Pixels[i];
                //var r = (float)px.R * 2f;
                //if (r > 255)
                //    r = 255;
                var r = px.R;
                var g = px.G;
                var b = px.B;

                if (g + b < 100 && r > 200)
                {
                    //px.R = b;
                    //px.B = g;
                    //px.G = r;

                    if (g > 50)
                        px.G = (byte)Math.Round((double)g * 0.888);
                    if (b > 50)
                        px.B = (byte)Math.Round((double)b * 0.888);

                    var nuValue = (double)r * 0.688;
                    px.R = (byte)Math.Round(nuValue);

                }

                //px.R = b;
                //px.B = g;
                //px.G = r;
            }
        }

        private void SwizR(PngRow row)
        {
            for (int i = 0; i < row.Count; i++)
            {
                var px = row.Pixels[i];
                //var r = (float)px.R * 2f;
                //if (r > 255)
                //    r = 255;
                var r = px.R;
                var g = px.G;
                var b = px.B;

                px.R = g;
                px.B = b;
                px.G = r;
            }
        }
    }
}
