﻿using ImageReader.Mafs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader
{
    public class Pixel
    {
        public Vec2 Position { get; set; }
        public Color Col { get; set; }

        public Pixel() { }

        public Pixel(Vec2 position, Color color)
        {
            Position = position;
            Col = color;
        }
    }
}
