﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Png
{
    public class CRC
    {
        public static void Reset()
        {
            crcTable = null;
        }
        //You can find a complete implementation of the CRC calculation(and PNG encoding in general) in this public domain code: http://upokecenter.dreamhosters.com/articles/png-image-encoder-in-c/

        static uint[] crcTable;

        // Stores a running CRC (initialized with the CRC of "IDAT" string). When
        // you write this to the PNG, write as a big-endian value
        public static uint idatCrc = Crc32(new byte[] { (byte)'I', (byte)'D', (byte)'A', (byte)'T' }, 0, 4, 0);
        public static uint ihdrCrc = Crc32(new byte[] { (byte)'I', (byte)'H', (byte)'D', (byte)'R' }, 0, 4, 0);
        public static uint iendCrc = Crc32(new byte[] { (byte)'I', (byte)'E', (byte)'N', (byte)'D' }, 0, 4, 0);

        // Call this function with the compressed image bytes, 
        // passing in idatCrc as the last parameter
        public static uint Crc32(byte[] stream, int offset, int length, uint crc)
        {
            uint c;
            if (crcTable == null)
            {
                crcTable = new uint[256];
                for (uint n = 0; n <= 255; n++)
                {
                    c = n;
                    for (var k = 0; k <= 7; k++)
                    {
                        if ((c & 1) == 1)
                            c = 0xEDB88320 ^ ((c >> 1) & 0x7FFFFFFF);
                        else
                            c = ((c >> 1) & 0x7FFFFFFF);
                    }
                    crcTable[n] = c;
                }
            }
            c = crc ^ 0xffffffff;
            var endOffset = offset + length;
            for (var i = offset; i < endOffset; i++)
            {
                c = crcTable[(c ^ stream[i]) & 255] ^ ((c >> 8) & 0xFFFFFF);
            }
            return c ^ 0xffffffff;
        }
    }
}
