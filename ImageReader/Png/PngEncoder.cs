﻿using ImageReader.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageReader.Png
{
    public class PngEncoder : IPngEncoder
    {
        private byte[] _result;
        public PngImage Image { get; set; }

        public byte[] Encode(PngImage image)
        {
            Image = image;
            EncodeImageAsPng();
            return _result;
        }

        private void EncodeImageAsPng()
        {
            //save an identical copy for debugging when done
            var idata = Image.Reconstitute();
            Compression.CompressData(idata, out byte[] compressed);
            //not too sure about crc data...
            var clen = compressed.Length;
            //all ints > 8bit need to be big endian
            var lbytes = BitConverter.GetBytes(clen).Reverse().ToArray();
            var h = "IDAT";
            var chunkNameBytes = Encoding.ASCII.GetBytes(h);

            var data = new List<byte>();
            data.AddRange(lbytes);
            data.AddRange(chunkNameBytes);
            data.AddRange(compressed);
            //and CRC?
            //Idk...
            CRC.Reset();
            var crc = CRC.Crc32(compressed, 0, compressed.Length, CRC.idatCrc);
            var crcBytes = BitConverter.GetBytes(crc).Reverse();
            //we may have to reverse for endianess consistency

            var headerBytes = CreateHeaderBytes();


            var finalResult = new List<byte>();
            //now take the heade info
            finalResult.AddRange(headerBytes);
            //add the length of the IDAT chunk
            finalResult.AddRange(lbytes);
            //add the name of the chunk
            finalResult.AddRange(chunkNameBytes);
            //add the compressed data
            finalResult.AddRange(compressed);
            //add the crc
            finalResult.AddRange(crcBytes);

            //now add the ImgEnd chunk
            var imgEndLen = BitConverter.GetBytes(0);
            var imgEndChunkName = Encoding.ASCII.GetBytes("IEND");
            var t = new byte[0];
            CRC.Reset();
            var imgEndCRC = CRC.Crc32(t, 0, 0, CRC.iendCrc);
            var imgendCrcBytes = BitConverter.GetBytes(imgEndCRC).Reverse();
            //var imgEndCRC = Encoding.ASCII.GetBytes("®B`‚");
            //var endBytes = new List<byte>();
            //endBytes.AddRange(headerBytes);
            //endBytes.AddRange(imgEndLen);
            //endBytes.AddRange(imgEndChunkName);
            //endBytes.AddRange(imgEndCRC);

            finalResult.AddRange(imgEndLen);
            finalResult.AddRange(imgEndChunkName);
            finalResult.AddRange(imgendCrcBytes);


            _result = finalResult.ToArray();

        }

        private byte[] CreateHeaderBytes()
        {
            var pngSignature = new byte[] { 137, 80 ,78, 71, 13, 10, 26, 10 };

            var hdrData = new List<byte>();
            var width = BitConverter.GetBytes(Image.Header.Width).Reverse().ToArray();
            hdrData.AddRange(width);
            var height = BitConverter.GetBytes(Image.Header.Height).Reverse().ToArray();
            hdrData.AddRange(height);
            var bd = BitConverter.GetBytes(Image.Header.BitDepth).First(k => k != 0);
            hdrData.Add(bd);
            var ct = BitConverter.GetBytes(Image.Header.ColorType).FirstOrDefault(k => k != 0);
            hdrData.Add(ct);
            var cm = BitConverter.GetBytes(Image.Header.CompressionMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(cm);
            var fm = BitConverter.GetBytes(Image.Header.FilterMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(fm);
            var im = BitConverter.GetBytes(Image.Header.InterlaceMethod).FirstOrDefault(k => k != 0);
            hdrData.Add(im);

            var len = hdrData.Count;
            var lenBytes = BitConverter.GetBytes(len).Reverse().ToArray();
            var ihdr = Encoding.ASCII.GetBytes("IHDR");

            //now 4 bytes for the crc... no idea wtf that is
            //https://stackoverflow.com/questions/24082305/how-is-png-crc-calculated-exactly
            //I believe I will simply copy and paste the code from this question
            CRC.Reset();
            var crc = CRC.Crc32(hdrData.ToArray(), 0, hdrData.Count, CRC.ihdrCrc);
            var crcBytes = BitConverter.GetBytes(crc).Reverse();//new

            var finalResult = new List<byte>();
            //now take the preceding header data
            finalResult.AddRange(pngSignature);
            //now add the len
            finalResult.AddRange(lenBytes);
            //now the chunk name
            finalResult.AddRange(ihdr);
            //now the data
            finalResult.AddRange(hdrData);
            //and now the crc
            finalResult.AddRange(crcBytes);

            return finalResult.ToArray();
        }
    }
}
