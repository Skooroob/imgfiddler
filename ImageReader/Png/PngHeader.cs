﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader
{
    public class PngHeader
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int BitDepth { get; set; }
        /// <summary>
        /// Color type codes represent sums of the following values: 1 (palette used), 2 (color used), and 4 (alpha channel used). Valid values are 0, 2, 3, 4, and 6.
        /// </summary>
        public int ColorType { get; set; }
        public int CompressionMethod { get; set; }
        public int FilterMethod { get; set; }
        public int InterlaceMethod { get; set; }

        //http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html
        ///  Color    Allowed    Interpretation
        ///     Type Bit Depths

        ///0       1,2,4,8,16  Each pixel is a grayscale sample.

        ///2       8,16        Each pixel is an R, G, B triple.

        ///3       1,2,4,8     Each pixel is a palette index;
        ///                    a PLTE chunk must appear.

        ///4       8,16        Each pixel is a grayscale sample,
        ///                    followed by an alpha sample.

        ///6       8,16        Each pixel is an R, G, B triple,
        ///                    followed by an alpha sample.
    }
}
