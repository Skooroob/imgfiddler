﻿using ImageReader.Mafs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Png
{
    public class PngImage
    {
        public string Name { get; set; }
        public PngHeader Header { get; set; }
        public List<PngRow> Grid { get; set; }
        public Dictionary<Vec2, Color> HashGrid { get; set; }

        public PngImage()
        {
            Grid = new List<PngRow>();
        }

        public void Hashup()
        {
            HashGrid = new Dictionary<Vec2, Color>();
            var bag = new ConcurrentBag<KeyValuePair<Vec2, Color>>();

            Parallel.For(0, Grid.Count, y => 
            {
                for (int x = 0; x < Grid[y].Pixels.Count; x++)
                {
                    var col = Grid[y].Pixels[x];
                    var position = new Vec2(x, y);
                    var pair = new KeyValuePair<Vec2, Color>(position, col);
                    bag.Add(pair);
                }
            });

            HashGrid = bag.ToDictionary(k => k.Key, k => k.Value);
            
            //for (int y = 0; y < Grid.Count; y++)
            //{
            //    for (int x = 0; x < Grid[y].Pixels.Count; x++)
            //    {
            //        var col = Grid[y].Pixels[x];
            //        var position = new Vec2(x, y);
            //        HashGrid.Add(position, col);
            //    }
            //}
        }

        /// <summary>
        /// Returns the uncompressed, unfiltered byte[] value of the pixels
        /// </summary>
        /// <returns></returns>
        public byte[] Reconstitute()
        {
            var data = new List<byte>();
            for (int i = 0; i < Grid.Count; i++)
            {
                data.Add((byte)0);
                data.AddRange(GetZeroBytes(Grid[i]));


                //if (Grid[i].DeltaConfiguration == 0)
                //{
                //    data.Add((byte)0);
                //    data.AddRange(GetZeroBytes(Grid[i]));
                //}
                //else if (Grid[i].DeltaConfiguration == 1)
                //{
                //    data.Add((byte)1);
                //    data.AddRange(GetOneBytes(Grid[i]));
                //}
                //else if (Grid[i].DeltaConfiguration == 2)
                //{
                //    data.Add((byte)2);
                //    data.AddRange(GetTwoBytes(Grid[i]));
                //}
            }
            return data.ToArray();
        }

        private byte[] GetZeroBytes(PngRow row)
        {
            var result = new List<byte>();

            foreach (var px in row.Pixels)
            {
                result.Add(px.R);
                result.Add(px.G);
                result.Add(px.B);
                //depending on color type...
                //and this is why jiraBots.png is fucked up!
                if(Header.ColorType == 6)
                    result.Add(px.A);

            }



            return result.ToArray();
        }

        private byte[] GetOneBytes(PngRow row)
        {
            throw new NotImplementedException();
        }

        private byte[] GetTwoBytes(PngRow row)
        {
            throw new NotImplementedException();
        }
    }
}
