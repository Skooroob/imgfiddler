﻿using ImageReader.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageReader.Png
{
    public class PngLoader : IPngLoader
    {
        #region Properties
        public PngImage Image { get; set; }
        public PngHeader ImgHeader { get; set; }
        public byte[] Data { get; set; }
        public int EndPosition { get; set; }
        private byte[] ImgEnd { get; set; }
        #endregion

        #region Ctor
        public PngLoader()
        {
            ImgEnd = Encoding.ASCII.GetBytes("IEND");
        }
        #endregion

        #region Interface Method
        public PngImage Load(byte[] data)
        {
            Data = data;
            FindEnd();
            if (Find("IHDR", out int position))
                GetImgHeaderData(position);
            else
                throw new Exception("Image header not found");
            if (Find("PLTE", out int plte))
                throw new NotImplementedException("Paletized colors not supported");
            if (FindMany("IDAT", out int[] positions))
                GetColors(positions);
            return Image;
        }
        #endregion
        
        #region Private Methods
       
        private void GetImgHeaderData(int startIndex)
        {
            ImgHeader = new PngHeader();
            startIndex++;
            var w = GetRange(startIndex, 4).Reverse().ToArray();
            ImgHeader.Width = BitConverter.ToInt32(w, 0);
            var h = GetRange(startIndex + 4, 4).Reverse().ToArray();
            ImgHeader.Height = BitConverter.ToInt32(h, 0);
            var bd = GetRange(startIndex + 8, 1).Reverse().ToArray();
            bd = Pad(bd);
            ImgHeader.BitDepth = BitConverter.ToInt32(bd, 0);
            var ct = GetRange(startIndex + 9, 1).Reverse().ToArray();
            ct = Pad(ct);
            ImgHeader.ColorType = BitConverter.ToInt32(ct, 0);
            var cm = GetRange(startIndex + 10, 1).Reverse().ToArray();
            cm = Pad(cm);
            ImgHeader.CompressionMethod = BitConverter.ToInt32(cm, 0);
            var fm = GetRange(startIndex + 11, 1).Reverse().ToArray();
            fm = Pad(fm);
            ImgHeader.FilterMethod = BitConverter.ToInt32(fm, 0);
            var im = GetRange(startIndex + 12, 1).Reverse().ToArray();
            im = Pad(im);
            ImgHeader.InterlaceMethod = BitConverter.ToInt32(im, 0);
        }

        private byte[] Pad(byte[] original)
        {
            var result = new byte[4];
            for (int i = 0; i < result.Length; i++)
                if (i < original.Length)
                    result[i] = original[i];

            return result;
        }

        private bool EndOfImage(int position)
        {
            if (position >= EndPosition)
                return true;
            else
                return false;
        }


        private void GetColors(int[] positions)
        {
            //The reason you got 120 for first value:
            //the IDAT is big endian, so needs to reversed before using. You were reading
            //cycle redund. check values

            //These are all off by 1, apparently
            //for (int i = 0; i < positions.Length; i++)
            //    positions[i]++;

            //FIXED: This causes a crc error in the ihdr chunk! 
            //now the problem is invalid row-filtering types (what I called delta config)
            //which, judging from the output of pngcheck.exe, are all over the place
            var greatData = new List<byte>();
            for (int i = 0; i < positions.Length; i++)
            {
                var pos = positions[i] - 8;//go back before the name of the chunk, plus 4 more for length
                var l = GetRange(pos, 4);
                l = l.Reverse().ToArray();
                var plen = BitConverter.ToInt32(l, 0);
                var data = GetRange(positions[i], plen);
                greatData.AddRange(data);
            }

            var idata = greatData;

            Compression.DecompressData(idata.ToArray(), out byte[] unc);
            idata = unc.ToList();

            var bpp = 2;
            if (ImgHeader.ColorType == 2)
                bpp = 3;
            else if (ImgHeader.ColorType == 6)
                bpp = 4;

            BytesToPixels(idata.ToArray(), bpp);
        }

        private void BytesToPixels(byte[] imgData, int bytesPerPixel)
        {
            var rowIndx = 0;

            var lastPx = new Color();

            var currentRow = new PngRow();
            var rows = new List<PngRow>();
            //var currentRow = new List<PngColor>();
            // var rows = new List<List<PngColor>>();

            // var bytesPerRow = ImgHeader.Width * bytesPerPixel;
            //byte[] lastByteRow = new byte[ImgHeader.Width * bytesPerPixel];
            //var currentByteRow = new byte[ImgHeader.Width * bytesPerPixel];

            byte rowFilter = 0;
            bool restart = false;
            for (int i = 0; i < imgData.Length; i += bytesPerPixel)
            {
                if (currentRow.Count % ImgHeader.Width == 0 && !restart)
                {
                    restart = true;
                    if (currentRow.Count > 0)
                    {
                        rows.Add(currentRow);
                        currentRow = new PngRow();
                    }
                    rowFilter = imgData[i];
                    currentRow.DeltaConfiguration = (int)rowFilter;
                    i -= (bytesPerPixel - 1);
                    rowIndx = 0;

                    //lastByteRow = currentByteRow;
                    //currentByteRow = new byte[ImgHeader.Width * bytesPerPixel];
                    continue;
                }
                

                //for (int p = i; p < i + bytesPerPixel; p++)
                //{
                //    currentByteRow[(rowIndx * bytesPerPixel) + (p - i)] = imgData[p];
                //}


                if (rowFilter == 0)
                {
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);

                    currentRow.Add(px);
                }
                else if (rowFilter == 1)
                {
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);
                    if (currentRow.Count > 0)
                    {
                        px = lastPx.Delta(px);
                    }
                    currentRow.Add(px);
                    lastPx = px;
                }
                else if (rowFilter == 2)
                {
                    //it turns out 2 is the same as one, difference being between the px ABOVE the current px

                    //just a copy of the last row
                    //however, since I'm not 100% sure of that
                    //I need to know if there's ever a case of 2 not being followed by all zeros

                    var priorPx = rows.Last().Pixels[rowIndx];
                    var px = new Color();
                    for (int k = i; k < i + bytesPerPixel; k++)
                        px.AddColor(imgData[k]);
                    px = priorPx.Delta(px);
                    currentRow.Add(px);


                    //working code - don't delete
                    //var copy = rows.Last().Duplicate();
                    //rows.Add(copy);
                    //i += (bytesPerPixel * (ImgHeader.Width - 1));
                }
                else if (rowFilter == 4)
                {
                    int a = 0;
                    int b = 0;
                    int c = 0;
                    var px = new Color();

                    //6 for first red val?
                    //have a look at the values
                    var rawX = imgData[i];
                    var ff = imgData[i + 1];
                    var fff = imgData[i + 2];
                   // var current = currentByteRow;

                    //First Paeth pixel
                    //Above: 48, 61, 31
                    //current: 41, 54, 24
                    //actual: 41, 245, 242

                    //TODO: Make this bitdepth independant (currently does not work with A channel)

                    for (int k = i; k < i + bytesPerPixel; k++)
                    {
                        var raw = imgData[k];
                        var inc = k - i;
                        if (rowIndx == 0)
                        {

                            a = 0;
                            c = 0;
                            var above = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                            if (inc == 0)
                            {
                                b = above.R;
                            }
                            else if (inc == 1)
                            {
                                b = above.G;
                            }
                            else if (inc == 2)
                            {
                                b = above.B;
                            }
                            else if (inc == 3)
                            {
                                b = above.A;
                            }
                        }
                        else
                        {
                            var leftPixel = currentRow.Pixels[currentRow.Pixels.Count - 1];
                            var above = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                            var aboveLeft = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count - 1];

                            if (inc == 0)
                            {
                                a = leftPixel.R;
                                b = above.R;
                                c = aboveLeft.R;
                            }
                            else if (inc == 1)
                            {
                                a = leftPixel.G;
                                b = above.G;
                                c = aboveLeft.G;
                            }
                            else if (inc == 2)
                            {
                                a = leftPixel.B;
                                b = above.B;
                                c = aboveLeft.B;
                            }
                            else if (inc == 2)
                            {
                                a = leftPixel.A;
                                b = above.A;
                                c = aboveLeft.A;
                            }
                        }

                        // p := a + b - c   //initial estimate
                        var p = a + b - c;
                        var pa = Math.Abs(p - a);
                        var pb = Math.Abs(p - b);
                        var pc = Math.Abs(p - c);

                        var aDiff = ByteDiff(raw, (byte)a);
                        var bDiff = ByteDiff(raw, (byte)b);
                        var cDiff = ByteDiff(raw, (byte)c);
                        
                        if (pa <= pb && pa <= pc)
                        {
                            //use a
                            var diff = ByteDiff(raw, (byte)a);
                            px.AddColor(diff);
                        }
                        else if (pb <= pc)
                        {
                            //use b
                            var diff = ByteDiff(raw, (byte)b);

                            px.AddColor(diff);
                        }
                        else
                        {
                            //use c
                            var diff = ByteDiff(raw, (byte)c);
                            px.AddColor(diff);
                        }

                    }

                    currentRow.Add(px);
                }
                else if (rowFilter == 3)
                {

                    var rawX = imgData[i];
                    var ff = imgData[i + 1];
                    var fff = imgData[i + 2];

                    var leftPixel = new Color();
                    var abovePixel = rows[rows.Count - 1].Pixels[currentRow.Pixels.Count];
                    //TODO: outfit this algo for different color types (i.e., 6)
                    //35th row

                    //33: 37, 46, 36
                    //34: 40, 52, 30

                    //everything goes to hell on line 34

                    var px = new Color();

                    if (rowIndx != 0)
                    {
                        leftPixel = currentRow.Pixels[currentRow.Pixels.Count - 1];
                    }

                    //The Average filter uses the average of the two neighboring pixels (left and above) to predict the value of a pixel.
                    // Average(x) = Raw(x) - floor((Raw(x-bpp)+Prior(x))/2)
                    for (int k = i; k < i + bytesPerPixel; k++)
                    {
                        var raw = imgData[k];
                        var inc = k - i;

                        byte l = 0;
                        byte a = 0;
                        if (inc == 0)
                        {
                            l = leftPixel.R;
                            a = abovePixel.R;
                        }
                        else if (inc == 1)
                        {
                            l = leftPixel.G;
                            a = abovePixel.G;
                        }
                        else if (inc == 2)
                        {
                            l = leftPixel.B;
                            a = abovePixel.B;
                        }
                        else if (inc == 3)
                        {
                            l = leftPixel.A;
                            a = abovePixel.A;
                        }

                        var avg = (l + a) / 2;
                        var val = ByteDiff(raw, (byte)avg);
                        px.AddColor((byte)val);
                    }

                    currentRow.Add(px);
                }
                else
                {
                    throw new NotImplementedException($"Filter type {rowFilter} not supported");
                }

                rowIndx++;
                restart = false;
            }
            rows.Add(currentRow);

            Image = new PngImage();
            Image.Header = ImgHeader;
            Image.Name = "Test";
            Image.Grid = rows;
        }
        #endregion

        #region Binary Search Methods
        private byte ByteDiff(byte a, byte b)
        {
            var start = (int)b;
            var diff = (int)a;
            var result = start;
            for (int i = start; i < start + diff; i++)
            {
                result++;
                if (result > 255)
                    result = 0;

            }
            return (byte)result;
        }

        private bool FindMany(string chunk, out int[] positions)
        {
            positions = null;
            var result = new List<int>();
            var query = Encoding.ASCII.GetBytes(chunk);
            var len = query.Length;
            for (int i = 0; i < Data.Length; i++)
            {
                if (EndOfImage(i))
                    break;

                var comp = GetRange(i, len);
                if (CompareBytes(query, comp))
                {
                    var start = i + len;
                    result.Add(start);
                }
            }

            if (result.Count > 0)
            {
                positions = result.ToArray();
                return true;
            }
            else
                return false;
        }

        private bool Find(string chunk, out int start)
        {
            start = 0;
            var query = Encoding.ASCII.GetBytes(chunk);
            var len = query.Length;
            for (int i = 0; i < Data.Length; i++)
            {
                if (EndOfImage(i))
                    break;

                var comp = GetRange(i, len);
                if (CompareBytes(query, comp))
                {
                    start = i + len - 1;
                    return true;
                }
            }

            return false;
        }

        private void FindEnd()
        {
            for (int i = Data.Length - 5; i > 0; i--)
            {
                var comp = GetRange(i, 4);
                if (CompareBytes(comp, ImgEnd))
                {
                    EndPosition = i;
                    break;
                }
            }
        }

        private byte[] GetRange(int start, int len)
        {
            var result = new byte[len];
            var inc = 0;
            for (int i = start; i < start + len; i++)
            {
                result[inc] = Data[i];
                inc++;
            }
            return result;
        }

        private bool CompareBytes(byte[] A, byte[] B)
        {
            if (A.Length != B.Length)
                return false;

            for (int i = 0; i < A.Length; i++)
                if (A[i] != B[i])
                    return false;

            return true;
        }

        #endregion
    }
}
