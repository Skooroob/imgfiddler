﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Png
{
    public class PngRow
    {
        public List<Color> Pixels { get; set; }
        public int DeltaConfiguration { get; set; }
        public int Count { get; set; }

        public PngRow()
        {
            Pixels = new List<Color>();
        }

        public void Add(Color px)
        {
            Pixels.Add(px);
            Count++;
        }

        public PngRow Duplicate()
        {
            var copy = new PngRow();
            foreach (var px in Pixels)
                copy.Add(px.Duplicate());
            return copy;
        }

    }
}
