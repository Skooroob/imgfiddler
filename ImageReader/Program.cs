﻿using ImageReader.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var prog = new CommandLine();
            prog.Run();
        }
    }
}
