﻿namespace ImageReader.Roles
{
    public interface IDataAccess
    {
        byte[] GetData();

        void WriteData(byte[] data);
    }
}
