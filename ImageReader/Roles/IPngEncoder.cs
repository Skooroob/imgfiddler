﻿using ImageReader.Png;

namespace ImageReader.Roles
{
    public interface IPngEncoder
    {
        byte[] Encode(PngImage image);
    }
}
