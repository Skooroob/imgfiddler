﻿using ImageReader.Png;

namespace ImageReader.Roles
{
    public interface IPngLoader
    {
        PngImage Load(byte[] data);
    }
}
