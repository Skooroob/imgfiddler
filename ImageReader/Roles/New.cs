﻿using ImageReader.Composition;
using ImageReader.Png;
using ImageReader.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Roles
{
    public class New
    {
        public static IDataAccess DataAccess(bool test = false)
        {
            if (test)
                return new UseSchema();
            else
                return new PromptForPath();
        }

        public static IPngLoader ImageLoader()
        {
            return new PngLoader();
        }

        public static IPngEncoder ImageEncoder()
        {
            return new PngEncoder();
        }
    }
}
