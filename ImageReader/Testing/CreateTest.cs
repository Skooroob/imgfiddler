﻿using ImageReader.Manipulation;
using ImageReader.Png;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Testing
{
    public class CreateTest : IManipulator
    {
        public PngImage Run(PngImage source)
        {

            var appData = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\ImgFiddle";
            if (!Directory.Exists(appData))
                Directory.CreateDirectory(appData);

            Console.WriteLine("Enter path to test image");
            var originalPath = Console.ReadLine();
            Console.WriteLine("Enter output path");
            var outputPath = Console.ReadLine();
            var settings = new TestScheme(originalPath, outputPath);
            var str = JsonConvert.SerializeObject(settings, Formatting.Indented);



            var sPath = $@"{appData}\testSettings.json";
            File.WriteAllText(sPath, str);

            return source;
        }

        private string BinDirectory()
        {
            return System.IO.Path.GetDirectoryName(
      System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
        }


    }
}
