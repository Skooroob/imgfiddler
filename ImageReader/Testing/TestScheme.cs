﻿namespace ImageReader.Testing
{
    public class TestScheme
    {
        public string Path { get; set; }
        public string OutputPath { get; set; }

        public TestScheme()
        {

        }

        public TestScheme(string input, string output)
        {
            Path = input;
            OutputPath = output;
        }
    }
}
