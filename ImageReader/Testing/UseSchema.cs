﻿using ImageReader.Roles;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageReader.Testing
{
    public class UseSchema : IDataAccess
    {
        public TestScheme Test { get; set; }
        public string Filename { get; set; }

        public UseSchema()
        {
            Filename = "testSettings.json";
            SetPaths();
        }

        public UseSchema(string filename)
        {
            Filename = filename;
            SetPaths();
        }

        public byte[] GetData()
        {
            return File.ReadAllBytes(Test.Path);
        }

        public void WriteData(byte[] data)
        {
            File.WriteAllBytes(Test.OutputPath, data);
        }

        private void SetPaths()
        {
            var settingsPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\ImgFiddle\{Filename}";
            var str = File.ReadAllText(settingsPath);
            Test = JsonConvert.DeserializeObject<TestScheme>(str);
        }
    }
}
